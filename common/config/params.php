<?php
return [
	'app' => [
		'siteDomain' => 'http://etoservis.ru',
	  'staticsDomain' => 'http://static.etoservis.ru',
		'adminEmail' => 'mail@etoservis.ru',
		'robotEmail' => 'mail@etoservis.ru',
		'allowHtmlTags' => 'p,span,strong,ul,ol,li,em,u,strike,br,hr,img,a',
		'moreTag' => '<!--more-->',
		'morePattern' => '<p><!--more--></p>'
	],
	'components.db' => [
		'class' => 'yii\db\Connection',
		'dsn' => 'mysql:host=localhost;dbname=iphone',
		'username' => 'iphone',
		'password' => 'iphone',
		'charset' => 'utf8',
		'tablePrefix' => 'ug4k_'
	],
	'components.cache' => [
		'class' => 'yii\caching\FileCache',
		'cachePath' => '@root/cache',
		'keyPrefix' => '0fea6a13c52b4d4725368f24b045ca84'
	],
	'components.mail' => [
	    'class' => 'yii\swiftmailer\Mailer',
	    'viewPath' => '@common/mails',
	    'useFileTransport' => false
	]
];
