<?php
namespace common\modules\users;

use Yii;
use yii\base\Module;

/**
 * Общий модуль [[Users]]
 * Осуществляет всю работу с пользователями.
 */
class Users extends Module
{
	/**
	 * @var integer Количество записей на главной странице модуля.
	 */
	public $recordsPerPage = 20;

	/**
	 * @var integer ID супер-админа которого нельзя удалить.
	 */
	public $superAdminId = 1;
}