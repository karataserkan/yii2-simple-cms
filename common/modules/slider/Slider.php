<?php
namespace common\modules\slider;

use Yii;
use yii\base\Module;

/**
 * Общий-модуль [[Slider]]
 * Осуществляет всю работу с страницами на общей стороне
 */
class Slider extends Module
{
	/**
	 * @var array Массив доступных для загрузки расширений изображений.
	 */
	public $imageAllowedExtensions = ['jpg', 'jpeg', 'png', 'gif'];
	
	/**
	 * @param string $image Имя изображения.
	 * @return string Путь к папке где хранятся изображения слайдера.
	 */
	public function path($id = null, $image = null)
	{
		$path = '@root/statics/web/slider';
		if ($id !== null) {
			$path .= '/' . $id;
		}
		if ($image !== null) {
			$path .= '/' . $image;
		}
		return Yii::getAlias($path);
	}

	/**
	 * @param string $image Имя изображения.
	 * @return string Путь к временной папке где хранятся изображения слайдера или путь к конкретному изображению.
	 */
	public function tempPath($image = null)
	{
		$path = '@root/statics/tmp/slider';
		if ($image !== null) {
			$path .= '/' . $image;
		}
		return Yii::getAlias($path);
	}

	/**
	 * @param string $image Имя изображения.
	 * @return string URL к папке где хранится/хранятся изображение/я.
	 */
	public function pathUrl($id = null, $image = null)
	{
		$url = '/slider/';
		if ($id !== null) {
			$url .= $id . '/';
		}
		if ($image !== null) {
			$url .= $image;
		}
		if (isset(Yii::$app->params['staticsDomain'])) {
			$url = Yii::$app->params['staticsDomain'] . $url;
		}
		return $url;
	}
}