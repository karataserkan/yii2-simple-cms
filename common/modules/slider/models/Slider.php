<?php
namespace common\modules\slider\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use common\modules\users\models\User;
use common\behaviors\PurifierBehavior;
use common\extensions\fileapi\behaviors\UploadBehavior;
use common\modules\slider\models\query\SliderQuery;

/**
 * Модуль таблицы {{%pages}}
 *
 * @property integer $id ID
 * @property string $parent_id ID родительской страницы
 * @property string $title Заголовок
 * @property string $alias Алиас
 * @property string $content Контент
 * @property boolean $status_id Статус
 * @property integer $create_time Дата создания
 * @property integer $update_time Дата обновления
 */
class Slider extends ActiveRecord
{
	/**
	 * Статусы записей модели
	 */
	const STATUS_UNPUBLISHED = 0;
	const STATUS_PUBLISHED = 1;

	/**
	 * Позиции слайдера
	 */
	const POSITION_TOP_LEFT = 'tl';
	const POSITION_TOP_MIDDLE = 'tm';
	const POSITION_TOP_RIGHT = 'tr';
	const POSITION_MIDDLE_LEFT = 'ml';
	const POSITION_MIDDLE_MIDDLE = 'mm';
	const POSITION_MIDDLE_RIGHT = 'mr';
	const POSITION_BOTTOM_LEFT = 'bl';
	const POSITION__BOTTOM_MIDDLE = 'bm';
	const POSITION_BOTTOM_RIGHT = 'br';

	/**
	 * @var yii\db\ActiveRecord array Массив моделей загружаемых изображений
	 */
	protected $_items;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%slider}}';
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		$behaviors = [
			'purifierBehavior' => [
			    'class' => PurifierBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_UPDATE => ['content'],
					ActiveRecord::EVENT_BEFORE_INSERT => ['content'],
				],
				'textAttributes' => [
					ActiveRecord::EVENT_BEFORE_UPDATE => ['title'],
					ActiveRecord::EVENT_BEFORE_INSERT => ['title'],
				]
			]
		];

		return $behaviors;
	}

	/**
	 * @inheritdoc
	 */
	public static function find()
    {
        return new SliderQuery(get_called_class());
    }

	/**
	 * Выбор записи по [[id]]
	 * @param integer $id
	 */
	public static function findIdentity($id)
	{
		return static::findOne($id);
	}

	/**
	 * Выбор записи по [[id]]
	 * @param array $id
	 */
	public static function findMultipleIdentity($id)
	{
		return static::find()->where(['id' => $id])->all();
	}

	/**
	 * @return array Массив с статусами страниц
	 */
	public static function getStatusArray()
	{
		return [
			self::STATUS_PUBLISHED => 'Опубликованно',
		    self::STATUS_UNPUBLISHED => 'Неопубликованно'
		];
	}

	/**
	 * @return array Массив с статусами страниц
	 */
	public static function getPositionArray()
	{
		return [
		    self::POSITION_TOP_LEFT => 'Верхний левый угол',
		    self::POSITION_TOP_MIDDLE => 'Верхний центр',
		    self::POSITION_TOP_RIGHT => 'Верхний правый угол',
		    self::POSITION_MIDDLE_LEFT => 'Средний левый угол',
		    self::POSITION_MIDDLE_MIDDLE => 'Центр',
		    self::POSITION_MIDDLE_RIGHT => 'Средний правый угол',
		    self::POSITION_BOTTOM_LEFT => 'Нижний левый угол',
		    self::POSITION__BOTTOM_MIDDLE => 'Нижниый центр',
		    self::POSITION_BOTTOM_RIGHT => 'Нижниый правый угол'
		];
	}

	/**
	 * @return string Читабельный статус поста.
	 */
	public function getStatus()
	{
		$status = self::getStatusArray();
		return $status[$this->status_id];
	}

	/**
	 * @return string Читабельная позиция слайдера.
	 */
	public function getPosition()
	{
		$status = self::getPositionArray();
		return $status[$this->position_id];
	}

	public function setItems(array $value)
	{
		$this->_items = $value;
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['title', 'string', 'max' => 255],
			['status_id', 'in', 'range' => array_keys(self::getStatusArray())],
			[['ordering'], 'integer', 'integerOnly' => true],
			['ordering', 'default', 'value' => 0],
			['status_id', 'default', 'value' => self::STATUS_PUBLISHED],
			['position_id', 'default', 'value' => self::POSITION_TOP_LEFT],
			['position_id', 'in', 'range' => array_keys(self::getPositionArray())]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		return [
			'admin-create' => ['title', 'content', 'image_url', 'position_id', 'status_id', 'ordering'],
			'admin-update' => ['title', 'content', 'image_url', 'position_id', 'status_id', 'ordering'],
			'delete-image' => ''
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'title' => 'Заголовок',
			'content' => 'Текст',
			'position_id' => 'Позиция',
			'ordering' => 'Номер',
			'status_id' => 'Статус',
			'items' => 'Изображения'
		];
	}

	/**
	 * @return \yii\db\ActiveRelation Изображения галереи
	 */
	public function getImages()
	{
		return $this->hasMany(Image::className(), ['slider_id' => 'id']);
	}

	/**
	 * @inheritdoc
	 */
	public function afterSave($insert)
	{
		$this->saveItems();
		parent::afterSave($insert);
	}

	/**
	 * @inheritdoc
	 */
	public function beforeDelete()
	{
		if (parent::beforeDelete()) {
			if ($models = Image::find()->where(['slider_id' => $this->id])->all()) {
				foreach ($models as $model) {
					$model->delete();
				}
			}
			FileHelper::removeDirectory(Yii::$app->getModule('slider')->path($this->id));
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Сохраняем изображения галереи
	 */
	protected function saveItems()
	{
		if ($this->_items && is_array($this->_items)) {
			$module = Yii::$app->getModule('slider');
			foreach ($this->_items as $image) {
				if (is_file($module->tempPath($image->name))) {
					$image->slider_id = $this->id;
					if ($image->save(false) && FileHelper::createDirectory($module->path($this->id))) {
						rename($module->tempPath($image->name), $module->path($this->id, $image->name));
					}
				}
			}
			FileHelper::removeDirectory($module->tempPath());
		}
	}
}