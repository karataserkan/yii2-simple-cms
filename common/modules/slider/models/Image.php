<?php
namespace common\modules\slider\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use common\extensions\fileapi\behaviors\UploadBehavior;
use common\modules\slider\models\query\ImageQuery;

/**
 * Модуль таблицы {{%pages}}
 *
 * @property integer $id ID
 * @property string $parent_id ID родительской страницы
 * @property string $title Заголовок
 * @property string $alias Алиас
 * @property string $content Контент
 * @property boolean $status_id Статус
 * @property integer $create_time Дата создания
 * @property integer $update_time Дата обновления
 */
class Image extends ActiveRecord
{
	/**
	 * @var string Полный URL до изображения.
	 */
	protected $_url;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%slider_image}}';
	}

	/**
	 * @inheritdoc
	 */
	public static function find()
    {
        return new ImageQuery(get_called_class());
    }

	/**
	 * Выбор записи по [[id]]
	 * @param integer $id
	 */
	public static function findIdentity($id)
	{
		return static::findOne($id);
	}

	/**
	 * Выбор записи по [[id]]
	 * @param array $id
	 */
	public static function findMultipleIdentity($id)
	{
		return static::find()->where(['id' => $id])->all();
	}

	/**
	 * @return string|null Полный URL адрес до изображения поста.
	 */
	public function getUrl()
	{
		if ($this->_url === null) {
			$this->_url = Yii::$app->getModule('slider')->pathUrl($this->slider_id, $this->name);
		}
		return $this->_url;
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['name', 'required', 'on' => 'admin-create'],
			['name', 'match', 'pattern' => '/^[a-z0-9]+.+[a-z]+$/iu'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		return [
			'admin-create' => ['name'],
			'admin-update' => '',
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'slider_id' => 'Слайдер',
			'name' => 'Изображение'
		];
	}

	/**
	 * @return \yii\db\ActiveRelation Галерея.
	 */
	public function getSlider()
	{
		return $this->hasOne(Slider::className(), ['id' => 'slider_id']);
	}

	/**
	 * @inheritdoc
	 */
	public function beforeDelete()
	{
		if (parent::beforeDelete()) {
			$module = Yii::$app->getModule('slider');
			$image = $module->path($this->slider_id, $this->name);
			if (is_file($image)) {
				unlink($image);
			}
			return true;
		} else {
			return false;
		}
	}
}