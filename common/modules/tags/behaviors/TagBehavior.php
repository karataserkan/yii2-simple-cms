<?php
namespace common\modules\tags\behaviors;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\validators\Validator;
use common\modules\tags\helpers\TagsHelper;
use common\modules\tags\models\Tag;
use common\modules\tags\models\TagModel;

/**
 * Данное поведение позволяет подключать функционал добавления тэгов к любой сущности.
 *
 * Пример подключения:
 * ...
 * 'tagBehavior' => [
 *     'class' => 'common\modules\tags\behaviors\TagBehavior',
 *     'scenarios' => ['create', 'update']
 * ]
 * ...
 */
class TagBehavior extends Behavior
{
	/**
	 * @var array Scenarios
	 */
	public $scenarios = [];

	/**
	 * @var string Список всех тэгов модели.
	 */
	protected $_tagArray;

	/**
	 * @var array Массив идентификаторов тэгов.
	 */
	protected $_tagIds = [];

	/**
	 * @var array Массив идентификаторов текущийх тэгов модели.
	 */
	protected $_oldTagIds;

	/**
	 * @var array Массив новых тэгов.
	 */
	protected $_tags = [];

	/**
	 * @inheritdoc
	 */
	public function attach($owner)
	{
		parent::attach($owner);

		// Создаём правило валидации входных тэгов.
		$validator = Validator::createValidator('validateTags', $this, ['tagArray']);
        $owner->validators[] = $validator;
	}

	/**
	 * @inheritdoc
	 */
	public function events()
	{
		return [
			ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
			ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
			ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete'
		];
	}

	/**
	 * @return integer Системный ID модели.
	 */
	public function getModelClassId()
	{
		$owner = $this->owner;
		$className = $owner::className();
		return TagsHelper::crcClassName($className);
	}

	/**
	 * @return array Массив старых тэгов модели.
	 */
	public function getOldTagIds()
	{
		if ($this->_oldTagIds === null) {
			if ($this->owner->tags) {
				$oldTagIds = [];
				foreach ($this->owner->tags as $tag) {
					$oldTagIds[] = $tag->id;
				}
				$this->_oldTagIds = $oldTagIds;
			}
		}
		return $this->_oldTagIds;
	}

	/**
	 * @return array Массив текущийх тэгов модели.
	 */
	public function getTagArray()
	{
		if ($this->_tagArray === null && $this->oldTagIds !== null) {
			$this->_tagArray = implode(',', $this->oldTagIds);
		}
		return $this->_tagArray;
	}

	/**
	 * Задаём значение текущийх тэгов модели.
	 */
	public function setTagArray($value)
	{
		if (is_string($value)) {
			$this->_tagArray = $value;
		}
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getTags()
	{
		return $this->owner->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable(TagModel::tableName(), ['model_id' => 'id'], function($query) {
			$query->where(['model_class_id' => $this->getModelClassId()]);
		});
	}

	public function validateTags()
	{
        //print_r($this->owner->tags);
        //die();
		if (!empty($this->tagArray)) {
			$query = new Query;
			$hasErrors = false;
			$tagIds = $tags = [];
			if (is_string($this->owner->tagArray)) {
				$tagArray = explode(',', $this->owner->tagArray);
				foreach ($tagArray as $tag) {
					if (!empty($tag)) {
						if (is_numeric($tag)) {
							$tagIds[] = $tag;
						} elseif (is_string($tag)) {
							$tags[] = str_replace('{%#=#%}', '', $tag);
						} else {
							$hasErrors = true;
						}
					} else {
						$hasErrors = true;
					}
				}
				if (!empty($tagIds)) {
					$count = $query->from(Tag::tableName())
					               ->where(['id' => $tagIds])
					               ->count();
					if ($count != count($tagIds)) {
						$hasErrors = true;
					}
				}
			} else {
				$hasErrors = true;
			}
			if ($hasErrors !== false) {
				$this->owner->addError('tagArray', 'Список тэгов содержит недопустимое значение.');
			} else {
				if (!empty($tagIds)) {
					$this->_tagIds = $tagIds;
				}
				if (!empty($tags)) {
					$this->_tags = $tags;
				}
			}
		}
	}

	/**
	 * Заполняем атрибут model_id текущей модели, нужным значением
	 */
	public function afterSave()
	{
        $this->validateTags();

		if (empty($this->scenarios) || in_array($this->owner->scenario, $this->scenarios)) {
			$modelClassId = $this->getModelClassId();
			$values = [];

			// Сохраняем существующие тэги.
			if ($this->_tagIds !== $this->oldTagIds) {
				TagModel::deleteAll(['model_id' => $this->owner->id, 'model_class_id' => $modelClassId]);
				if (!empty($this->_tagIds)) {
					foreach ($this->_tagIds as $id) {
						$values[] = [$this->owner->id, $id, $modelClassId];
					}
				}
			}
			// Сохраняем новые тэги.
			if (!empty($this->_tags)) {
				foreach ($this->_tags as $tag) {
					$model = new Tag;
					$model->name = $tag;
					if ($model->save(false)) {
						$values[] = [$this->owner->id, $model->id, $modelClassId];
					}
				}
			}

			if (!empty($values)) {
				$this->owner->getDb()->createCommand()
				             ->batchInsert(TagModel::tableName(), ['model_id', 'tag_id', 'model_class_id'], $values)->execute();
			}
		}
	}

	/**
	 * Удаляем СЕО записи текущей модели в момент удаления самой модели
	 */
	public function beforeDelete()
	{
        $this->validateTags();
		if (empty($this->scenarios) || in_array($this->owner->scenario, $this->scenarios)) {
			TagModel::deleteAll(['model_id' => $this->owner->id, 'model_class_id' => $this->getModelClassId()]);
		}
	}
}
