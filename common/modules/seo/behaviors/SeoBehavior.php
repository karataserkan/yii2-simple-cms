<?php
namespace common\modules\seo\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\Query;
use common\modules\tags\helpers\TagsHelper;
use common\modules\seo\models\Seo;

/**
 * Данное поведение добавляет все необходимые методы и связи модели для работы с СЕО сущностями этой модели
 * 
 * Пример подключения:
 * ...
 * 'seoBehavior' => [
 *     'class' => 'common\modules\seo\behaviors\seoBehavior'
 * ]
 * ...
 */
class SeoBehavior extends Behavior
{
	/**
	 * Назначаем обработчик для [[owner]] событий.
	 * @return array события (array keys) с назначеными им обработчиками (array values).
	 */
	public function events()
	{
		return [
		    ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete'
		];
	}

	/**
	 * @return integer Системный ID модели.
	 */
	public function getModelClassId()
	{
		$owner = $this->owner;
		$className = $owner::className();		
		return TagsHelper::crcClassName($className);
	}

	/**
	 * @return \yii\db\ActiveRelation
	 */
	public function getSeo()
	{
    return $this->owner->hasOne(Seo::className(), array('model_id'=>'id'))->andWhere(['model_class_id' => $this->getModelClassId()]);
//		return $this->owner->hasOne(Seo::className(), ['model_id' => 'id'], function($query) {
//			$query->where(['model_class_id' => $this->getModelClassId()]);
//		});
	}

	/**
	 * Удаляем СЕО записи текущей модели в момент удаления самой модели
	 */
	public function beforeDelete()
	{
		$this->owner->seo->delete();
	}
}