<?php
namespace common\modules\menu\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\menu\models\Item;

/**
 * Общая модель поиска по постам блога
 */
class ItemSearch extends Model
{
	public $url;
	public $label;
	public $title;
	public $ordering;
	public $menu_id;
	public $parent_id;
	public $status_id;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['url', 'label', 'title'], 'string'],
			[['menu_id', 'parent_id'], 'number', 'integerOnly' => true],
			[['status_id'], 'boolean'],
		];
	}

	public function search($params)
	{
		$query = Item::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$this->addCondition($query, 'url', true);
		$this->addCondition($query, 'label', true);
		$this->addCondition($query, 'title', true);
		$this->addCondition($query, 'menu_id');
		$this->addCondition($query, 'parent_id');
		$this->addCondition($query, 'status_id');

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
