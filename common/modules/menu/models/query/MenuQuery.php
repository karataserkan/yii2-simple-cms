<?php
namespace common\modules\menu\models\query;

use yii\db\ActiveQuery;
use common\modules\menu\models\Menu;

/**
 * Class MenuQuery
 * @package common\modules\menu\models\query
 * Класс кастомных запросов модели [[Menu]]
 */
class MenuQuery extends ActiveQuery
{
	/**
	 * Выбираем только опубликованые меню.
	 * @param ActiveQuery $query
	 */
	public function published()
	{
		$this->andWhere(Menu::tableName() . '.status_id = :status', [':status' => Menu::STATUS_PUBLISHED]);
		return $this;
	}
}