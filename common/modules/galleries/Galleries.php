<?php
namespace common\modules\galleries;

use Yii;
use yii\base\Module;

/**
 * Общий модуль галерей
 */
class Galleries extends Module
{
	/**
	 * @var integer Ширина превьюшки.
	 */
	public $thumbWidth = 600;

	/**
	 * @var integer Высота превьюшки.
	 */
	public $thumbHeight = 450;

	/**
	 * @param string $image Имя изображения
	 * @return string Полный путь к временной папке где хранится изображение
	 */
	public function tempPath($image = null)
	{
		$path = '@root/statics/tmp/galleries/' . Yii::$app->getUser()->getId();
		if ($image !== null) {
			$path .= '/' . $image;
		}
		return Yii::getAlias($path);
	}

	/**
	 * @param integer $id ИД галереи к которой относится изображение
	 * @param string $image Имя изображения
	 * @return string Полный путь к папке где хранится изображение
	 */
	public function path($id = null, $image = null)
	{
		$path = '@root/statics/web/galleries';
		if ($id !== null) {
			$path .= '/' . $id;
		}
		if ($image !== null) {
			$path .= '/' . $image;
		}
		return Yii::getAlias($path);
	}

	/**
	 * @param integer $id ИД галереи к которой относится изображение
	 * @param string $image Имя изображения
	 * @return string Полный путь к папке где хранится изображение
	 */
	public function thumbPath($id = null, $image = null)
	{
		$path = '@root/statics/web/galleries';
		if ($id !== null) {
			$path .= '/' . $id;
		}
		$path .= '/thumbs';
		if ($image !== null) {
			$path .= '/' . $image;
		}
		return Yii::getAlias($path);
	}

	/**
	 * @param integer $id ИД галереи к которой относится изображение
	 * @param string $image Имя изображения
	 * @return string URL к папке где хранится изображение
	 */
	public function pathUrl($id = null, $image = null)
	{
		$url = '/galleries/';
		if ($id !== null) {
			$url .= $id . '/';
		}
		if ($image !== null) {
			$url .= $image;
		}
		if (isset(Yii::$app->params['staticsDomain'])) {
			$url = Yii::$app->params['staticsDomain'] . $url;
		}
		return $url;
	}

	/**
	 * @param integer $id ИД галереи к которой относится изображение
	 * @param string $image Имя изображения
	 * @return string URL к папке где хранится изображение
	 */
	public function thumbUrl($id = null, $image = null)
	{
		$url = '/galleries/';
		if ($id !== null) {
			$url .= $id . '/';
		}
		$url .= 'thumbs/';
		if ($image !== null) {
			$url .= $image;
		}
		if (isset(Yii::$app->params['staticsDomain'])) {
			$url = Yii::$app->params['staticsDomain'] . $url;
		}
		return $url;
	}
}