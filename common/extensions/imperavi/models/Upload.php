<?php 
namespace common\extensions\imperavi\models;

use Yii;
use yii\base\Model;

/**
 * Class Upload
 * @package backend\modules\upload\models
 *
 * @property string $files файлы
 */
class Upload extends Model
{
	/**
	 * @var string Название валидатора
	 */
	public $validator;

	/**
	 * @var array Типы дсотупных расширений файлов для загрузки
	 */
	public $types;

	/**
	 * Переменные используются для сбора пользовательской информации, но не сохраняются в базу
	 * @var yii\web\UploadedFile $files
	 */
	public $file;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		if ($this->validator === 'image') {
			$rules = [
			    ['file', 'image']
			];
		} else {
			$rules = [
			    ['file', 'file', 'types' => $this->types]
			];
		}
		return $rules;
	}

	/**
	 * Название атрибутов модели
	 * @return array
	 */
	public function attributeLabels()
	{
		return [
			'file' => Yii::t('app', 'Файл')
		];
	}
}