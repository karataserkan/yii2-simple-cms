<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Миграция создаёт все таблицы БД модуля [[Tags]]
 * Создаются 2 таблицы:
 * - {{%tags}} => в которой хранится основная информация о тэгах.
 * - {{%tags_models}} => в которой хранится информация о связях тэга и моделей.
 */
class m131115_074307_create_tags_tbl extends Migration
{
	public function up()
	{
		// Настройки MySql таблицы.
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

		// Создаём таблицу тэгов.
		$this->createTable('{{%tags}}', [
			'id' => Schema::TYPE_PK,
			'name' => Schema::TYPE_STRING . '(255) NOT NULL',
			'ordering' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0'
		], $tableOptions);

		$this->createIndex('ordering', '{{%tags}}', 'ordering');

		// Создаём таблицу связей тэга с моеделью.
		$this->createTable('{{%tag_model}}', [
			'tag_id' => Schema::TYPE_INTEGER . ' NOT NULL',
			'model_id' => Schema::TYPE_INTEGER . ' NOT NULL',
			'model_class_id' => Schema::TYPE_BIGINT . ' NOT NULL',
			'PRIMARY KEY (`tag_id`, `model_id`, `model_class_id`)'
		], $tableOptions);

		$this->addForeignKey('FK_tag_model_tag', '{{%tag_model}}', 'tag_id', '{{%tags}}', 'id', 'CASCADE', 'CASCADE');
	}

	public function down()
	{
		$this->dropTable('{{%tag_model}}');
		$this->dropTable('{{%tags}}');
	}
}
