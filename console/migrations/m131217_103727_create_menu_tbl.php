<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Миграция которая создает все таблицы БД которые относятся к меню
 */
class m131217_103727_create_menu_tbl extends Migration
{
	public function up()
	{
		// Настройки MySql таблицы
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

		// Создаём таблицу меню
		$this->createTable('{{%menu}}', array(
			'id' => Schema::TYPE_PK,
			'title' => Schema::TYPE_STRING . '(255) NOT NULL',
			'description' => Schema::TYPE_STRING . '(255) NOT NULL',
			'status_id' => 'tinyint(1) NOT NULL DEFAULT 1'
		), $tableOptions);
		
		$this->createIndex('status_id', '{{%menu}}', 'status_id');

		// Создаём таблицу пунтков меню
		$this->createTable('{{%menu_items}}', array(
			'id' => Schema::TYPE_PK,
			'parent_id' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
			'menu_id' => Schema::TYPE_INTEGER . ' NOT NULL',
			'url' => Schema::TYPE_STRING . '(255) NOT NULL',
			'label' => Schema::TYPE_STRING . '(255) NOT NULL',
			'title' => Schema::TYPE_STRING . '(255) NOT NULL',
			'description' => Schema::TYPE_STRING . '(255) NOT NULL',
			'ordering' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
			'status_id' => 'tinyint(1) NOT NULL DEFAULT 1'
		), $tableOptions);

		$this->createIndex('menu_id', '{{%menu_items}}', 'menu_id');
		$this->createIndex('parent_id', '{{%menu_items}}', 'menu_id');
		$this->createIndex('ordering', '{{%menu_items}}', 'ordering');
		$this->createIndex('status_id', '{{%menu_items}}', 'status_id');
		$this->addForeignKey('FK_menu_item_menu', '{{%menu_items}}', 'menu_id', '{{%menu}}', 'id', 'CASCADE', 'CASCADE');
	}

	public function down()
	{
		$this->dropTable('{{%menu}}');
	}
}
