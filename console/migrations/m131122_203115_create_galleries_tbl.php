<?php

use yii\db\Schema;
use yii\db\Migration;
use common\modules\tags\helpers\TagsHelper;
use common\modules\galleries\models\Image;

/**
 * Миграция которая создает все таблицы БД которые относятся к галереи
 */
class m131122_203115_create_galleries_tbl extends Migration
{
	public function up()
	{
		// Настройки MySql таблицы
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

		// Добавляем запись в таблицу моделей
		$modelClass = Image::className();
		$modelClassId = TagsHelper::crcClassName($modelClass);
		$modelClass = addslashes($modelClass);
		$this->execute("INSERT INTO {{%models}} (`model_class`, `model_class_id`) VALUES ('$modelClass', '$modelClassId')");

		// Создаём таблицу категорий
		$this->createTable('{{%galleries_categories}}', array(
			'id' => Schema::TYPE_PK,
			'title' => Schema::TYPE_STRING . '(100) NOT NULL',
			'ordering' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
			'status_id' => 'tinyint(1) NOT NULL DEFAULT 1'
		), $tableOptions);

		$this->createIndex('status_id', '{{%galleries_categories}}', 'status_id');
		$this->createIndex('ordering', '{{%galleries_categories}}', 'ordering');

		// Создаём таблицу галерей
		$this->createTable('{{%galleries}}', array(
			'id' => Schema::TYPE_PK,
			'category_id' => Schema::TYPE_INTEGER . ' NOT NULL',
			'title' => Schema::TYPE_STRING . '(100) NOT NULL',
			'ordering' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
			'status_id' => 'tinyint(1) NOT NULL DEFAULT 1',
			'create_time' => Schema::TYPE_INTEGER . ' NOT NULL',
			'update_time' => Schema::TYPE_INTEGER . ' NOT NULL'
		), $tableOptions);

		$this->createIndex('category_id', '{{%galleries}}', 'category_id');
		$this->createIndex('status_id', '{{%galleries}}', 'status_id');
		$this->createIndex('create_time', '{{%galleries}}', 'create_time');
		$this->createIndex('update_time', '{{%galleries}}', 'update_time');
		$this->addForeignKey('FK_galleries_category', '{{%galleries}}', 'category_id', '{{%galleries_categories}}', 'id', 'CASCADE', 'CASCADE');

		// Создаём таблицу изображений
		$this->createTable('{{%gallery_image}}', array(
			'id' => Schema::TYPE_PK,
			'gallery_id' => Schema::TYPE_INTEGER . ' NOT NULL',
			'name' => Schema::TYPE_STRING . '(32) NOT NULL',
			'title' => 'text NOT NULL',
			'snippet' => 'text NOT NULL',
			'description' => 'text NOT NULL',
			'ordering' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
			'last' => 'tinyint(1) NOT NULL DEFAULT 0',
			'last_url' => Schema::TYPE_STRING . '(255) NOT NULL',
			'create_time' => Schema::TYPE_INTEGER . ' NOT NULL',
			'update_time' => Schema::TYPE_INTEGER . ' NOT NULL'
		), $tableOptions);

		$this->createIndex('gallery_id', '{{%gallery_image}}', 'gallery_id');
		$this->createIndex('ordering', '{{%gallery_image}}', 'ordering');
		$this->addForeignKey('FK_galleries_images_gallery', '{{%gallery_image}}', 'gallery_id', '{{%galleries}}', 'id', 'CASCADE', 'CASCADE');
	}

	public function down()
	{
		$this->dropTable('{{%gallery_image}}');
		$this->dropTable('{{%galleries}}');
		$this->dropTable('{{%galleries_categories}}');
	}
}
