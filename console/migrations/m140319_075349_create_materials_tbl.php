<?php

use yii\db\Schema;
use yii\db\Migration;

class m140319_075349_create_materials_tbl extends Migration
{
    public function up()
	{
		// Настройки MySql таблицы
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

		// Создаём таблицу материалов
		$this->createTable('{{%materials}}', [
			'id' => Schema::TYPE_PK,
			'parent_id' => Schema::TYPE_INTEGER . ' NOT NULL',
			'title' => Schema::TYPE_STRING . '(100) NOT NULL',
			'image_url' => Schema::TYPE_STRING . '(64) NOT NULL',
			'content' => 'text NOT NULL',
			'color' => Schema::TYPE_STRING . '(100) NOT NULL',
			'status_id' => 'tinyint(1) NOT NULL DEFAULT 1',
			'ordering' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
			'create_time' => Schema::TYPE_INTEGER . ' NOT NULL',
			'update_time' => Schema::TYPE_INTEGER . ' NOT NULL'
		], $tableOptions);

		$this->createIndex('create_time', '{{%materials}}', 'create_time');
		$this->createIndex('update_time', '{{%materials}}', 'update_time');
	}

	public function down()
	{
		$this->dropTable('{{%materials}}');
	}
}
