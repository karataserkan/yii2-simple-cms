<?php
/**
 * Страница всех галлерей
 * @var yii\base\View $this
 * @var backend\modules\galleries\models\Gallery $models
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\BaseUrl;
use yii\helpers\Json;
use frontend\modules\site\FancyBoxAsset;
use frontend\modules\site\LikeAsset;

$this->title = 'Галерея работ';
$this->params['pageClass'] = 'gallery';
$this->params['color'] = true;

$this->params['metaDescription'] = 'Моддинг и тюнинг iPhone — фотографии работ.';
$this->params['metaKeywords'] = 'Моддинг, тюнинг, iphone, 5s, 5, золото, porsche, дерево, кожа, крокодил, наппа, алькантара, Range Rover, Lamborghini, Ferrari';
$this->registerJsFile("//yastatic.net/share/share.js");
$c = 0;
?>
<style>
  #wrap_galleray_button_hide{

    position: fixed;
    left: 0px;
    height: 100%;
    width: 30px;
    cursor: pointer;
    margin-top: -80px;

  }
</style>
<div class="galleray_main">
  <div id="galleray_button">
    <div id="wrap_galleray_button_hide">
      <div id="galleray_button_hide"></div>
    </div>
  </div>
  <div class="galleray_left">
    <div id="galleray_left_menu">
      <div class="galleray_left_title">ПРОСМОТР ПО МОДЕЛЯМ:</div>
      <div class="galleray_menu">
        <a href="<?= Url::toRoute(['', 'tag' => Yii::$app->request->get('tag', "")]); ?>" class="g_item <?php echo (!$category_id)? 'active' :''; ?>">
          <div class="galleray_ml">
            <div class="text_m">Все модели</div>
            <div class="sub_text_m">2010-по н.в.</div>
          </div>
          <div class="galleray_mr">
            <span><?= $amount_img ?></span>
          </div>
          <div style="clear: both;"></div>
        </a>
        <?php if($category):?>
          <?php foreach($category as $model): ?>
            <a href="<?= Url::toRoute(['', 'id' => $model['id'], 'tag' => Yii::$app->request->get('tag', "")]); ?>" class="g_item <?php echo ($model['id'] == $category_id)? 'active' :''; ?>">
              <div class="galleray_ml">
                <div class="text_m"><?= $model['title'] ?></div>
                <div class="sub_text_m"><?= $model['sub_title'] ?></div>
              </div>
              <div class="galleray_mr">
                <span><?= isset($count_img[$model['id']])?$count_img[$model['id']]:0; ?></span>
              </div>
              <div style="clear: both;"></div>
            </a>
          <?php endforeach; ?>
        <?php endif; ?>

      </div>

      <div id="tags_sort" <?php echo (Yii::$app->request->get('tag', false))?'':'style="display:none;"'; ?>>
        <div id="border_top">
          <span id="close_t">x</span>
        </div>
        <?php if(count($tags)>0): ?>
          <div class="text_m">Показать результаты<br /> по тегу:</div>
          <div id="tag_list">
            <?php foreach($tags as $id_tag => $t): ?>
              <span><a href="<?= Url::toRoute(['/search/'.$t]); ?>" class="del"><?php echo $t; ?></a><span class="delete_tag">x</span></span>
            <?php endforeach; ?>
          </div>
        <?php endif; ?>
        <div class="text_m" style="margin-top: 20px;">Просмотр по тегам: <span id="refresh_tags"></span></div>
        <div id="lag_main_list">
          <?php foreach($tag_list as $t): ?>
            <div>
              <a href="<?= Url::toRoute(['', 'id' => Yii::$app->request->get('id', ""), 'tag' => $t->name]); ?>"><? echo $t->name; ?></a>
              <a style="text-decoration: none; opacity: 0.7;" href="<?= Url::toRoute(['', 'id' => Yii::$app->request->get('id', ""), 'tag' => Yii::$app->request->get('tag', "").",".$t->name]); ?>">+</a>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
      <div id="ts_menu"  <?php echo (Yii::$app->request->get('tag', false))?'style="display:none;"':''; ?>></div>
      <div class="white_sharing">
        <div id="white_sharing_close"></div>
        <div class="white_sharing_text">
          Подпишитесь и не
          пропустите новые
          фотографии:
        </div>
        <div style="padding: 15px">
          <script type="text/javascript" src="//vk.com/js/api/openapi.js?115"></script>

          <!-- VK Widget -->
          <div id="vk_subscribe"></div>
          <script type="text/javascript">
            VK.Widgets.Subscribe("vk_subscribe", {soft: 1}, -24617926);
          </script>
        </div>
        <div style="padding: 15px">
          <div id="fb-root"></div>
          <script>(function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) return;
              js = d.createElement(s); js.id = id;
              js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&appId=383174268480222&version=v2.0";
              fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
          <div class="fb-like" data-href="<?php echo BaseUrl::base(); ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>
        </div>
        <div style="padding: 15px">
          <a href="https://twitter.com/twitter" class="twitter-follow-button" data-show-count="false" data-lang="ru">Читать @twitter</a>
          <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
        </div>
      </div>

        <div class="white_sharing_button" <?php echo (Yii::$app->request->get('tag', false))?'style="display:none;"':''; ?>>
          Подписаться
        </div>
      </div>
  </div>
  <div class="galleray_content">
  <?php
  $fancyboxModels = [];
  if ($models) :
    ?>
        <div class="gallery">
          <?php foreach ($models as $image) :
              $fancyboxModels[$image->url] = [
                  'title' => $image['title'],
                  'content' => $image['description'],
                  'tags' => '',
                  'like' => $image['like'],
                  'views' => $image['views'],
                  'id' => $image['id']
              ]; ?>
              <?php if ($image->tags) {
                  foreach ($image->tags as $key => $tag) {
                      if ($key !== 0) {
                          $fancyboxModels[$image->url]['tags'] .= ', ';
                      }
                      $fancyboxModels[$image->url]['tags'] .= Html::a('#' . $tag['name'], ['/gallery/', 'tag' => $tag['name']]);
                  }
              } ?>
              <a href="<?= $image->url ?>" class="fancybox" rel="group" target="_blank" data-hash="<?= $image['id'] ?>">
                  <img <?= (++$c>8)?'class="lazy" data-':'' ?>src="<?= $image->thumb ?>" alt="<?= $image['title'] ?>" title="<?= $image['title'] ?>" />
              </a>
          <?php endforeach; ?>
        </div>
  <?php endif; ?>
</div>
<div style="clear: both;"></div>
</div>


<div id="over"></div>

<?php
    FancyBoxAsset::register($this);
    LikeAsset::register($this);
    $fancyboxModels = json_encode($fancyboxModels, JSON_UNESCAPED_UNICODE);
    $this->registerJs('var fancyboxModels = ' . $fancyboxModels . ';

        jQuery(".fancybox").fancybox({
            helpers: {
                title: {
                    type: "inside"
                }
            },
            padding: 0,
            margin: 0,
            afterLoad: function(current, previous) {
                if(localStorage.getItem("show_left_menu") > 0){
                    $(".fancybox-overlay-fixed").css("margin-left", "260px");
                }
                var like = fancyboxModels[this.href]["like"];
                if(localStorage.getItem("like_"+fancyboxModels[this.href]["id"]) == "1"){
                  //$("#button_like span.icon_eye").style.setProperty("color", "#fd5a42", "important");
                  //$("#like_count").text(parseInt($("#like_count").text())+1);
                  like = like+1;
                }
                fancyboxModels[this.href]["views"] = fancyboxModels[this.href]["views"]+1;
                $(".white_sharing_button").css("display", "none");
                $(".logo").css("display", "none");
                var active = localStorage.getItem("bottom_menu");
                var content = "<div class=\"fancybox-info\" style=\"bottom:"+(active == "active"?"0px":"-125px")+"\"><h1>" + fancyboxModels[this.href]["title"] + "<span id=\"full_menu_hide\" class=\""+active+"\"></span></h1>";
                if (!fancyboxModels[this.href]["content"]) {
                fancyboxModels[this.href]["content"] = "&nbsp;";
                }
                    content +="<div class=\"left_div\">" + fancyboxModels[this.href]["content"] + "</div><div class=\"div_sep\"></div>";

                if (fancyboxModels[this.href]["tags"]) {
                    content += "<div class=\"tags\"><span>Дугие телефоны с тегами:</span><p> " + fancyboxModels[this.href]["tags"] + "</p></div>";
                }

                content += "<div id=\"share_block\">"+
                "<div id=\"button_like\"><span data-imgid=\""+fancyboxModels[this.href]["id"]+"\" class=\"icon icon_eye\">"+
                "<svg xmlns=\"http://www.w3.org/2000/svg\" class=\"svg-icon\" width=\"19\" height=\"14\"><path fill=\"currentColor\" fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M9,4.561c-1.08,0-1.96,0.96-1.96,2.06c0,1.131,0.88,2.04,1.96,2.04 c1.061,0,1.98-0.909,1.98-2.04C10.98,5.521,10.061,4.561,9,4.561z M13.41,6.62c0-2.51-1.99-4.57-4.41-4.57S4.59,4.11,4.59,6.62 c0,2.52,1.99,4.58,4.41,4.58S13.41,9.14,13.41,6.62z M18,6.62c0,0-3.62,6.49-9,6.49S0,6.62,0,6.62s3.62-6.51,9-6.51S18,6.62,18,6.62 z\"></path></svg>"+
                "</span><span class=\"ghost-button__counter\">"+fancyboxModels[this.href]["views"]+"</span></div>"+
                "<div id=\"button_like_img\">"+
                "<span class=\"icon icon_heart\"><svg xmlns=\"http://www.w3.org/2000/svg\" class=\"svg-icon\" width=\"16.998\" height=\"14\"><path fill-rule=\"evenodd\" clip-rule=\"evenodd\" fill=\"currentColor\" d=\"M12.398.04c-.257-.025-.498-.04-.723-.04-1.812 0-2.681 1.346-3.676 2.348-.993-1.002-1.863-2.348-3.674-2.348-.226 0-.468.015-.724.04-1.569.127-3.381 1.345-3.601 3.694v.773c.206 2.247 2.192 5.015 7.999 8.493 5.808-3.478 7.796-6.246 8.001-8.493v-.773c-.221-2.349-2.034-3.567-3.602-3.694z\"></path></svg></span>"+
                "<span id=\"like_count\">"+like+"</span></div>"+
                "<div id=\"button_share\"><span class=\"icon icon_share\">"+
                "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"12\" height=\"17\"><path fill=\"currentColor\" fill-rule=\"evenodd\" clip-rule=\"evenodd\" d=\"M6.98 12h-1.98v-6h-3l4-6 4 6h-3l-.02 6zm-6.98 5v-9h2l-.02 7h8.02v-7h2v9h-12z\"></path></svg>"+
                "</span><span class=\"button-content\">Поделиться</span></div></div>"+
                "<div id=\"share_list\">"+
                  "<div>"+
                    "<div id=\"share_list_block\" class=\"social-likes social-likes_vertical social-likes_light\">"+
                     " <div class=\"facebook\" title=\"Поделиться ссылкой на Фейсбуке\">Facebook</div>"+
                      "<div class=\"twitter\" title=\"Поделиться ссылкой в Твиттере\">Twitter</div>"+
                      "<div class=\"vkontakte\" title=\"Поделиться ссылкой во Вконтакте\">Вконтакте</div>"+
                      "<div class=\"odnoklassniki\" title=\"Поделиться ссылкой в Одноклассниках\">Одноклассники</div>"+
                    "</div>"+
                  "</div>"+
                  "<div class=\"popup-tail\"></div>"+
                "</div>";

                content += "</div>";

                $.extend(this, {
                    aspectRatio: false,
                    type: "html",
                    width: "100%",
                    height: "100%",
                    content: "<div class=\"fancybox-image\" style=\"background-image:url(" + this.href + ");\">" + content + "</div>"
                });
                if($("#galleray_button_hide").hasClass("show_b")){
                  $(".fancybox-overlay-fixed").css("margin-left", "30px");
                }
            },
            beforeShow: function() {
                window.location.hash = this.element.data("hash");
                $.ajax({
                  url: "",
                  type: "POST",
                  data: {id: $("#button_like span.icon_eye").attr("data-imgid")},
                });
                $(".fancybox-info h1").on("click", function(){
                  if($("#full_menu_hide").hasClass("active")){
                    $("#full_menu_hide").removeClass("active");
                    $(".fancybox-info").animate({bottom: "-125px"}, 700);
                    localStorage.setItem("bottom_menu", "");
                  }
                  else{
                    $("#full_menu_hide").addClass("active");
                    $(".fancybox-info").animate({bottom: "0px"}, 700);
                    localStorage.setItem("bottom_menu", "active");
                  }
                });
                $("#button_like_img span.icon_heart").on("click", function(){
                  if(localStorage.getItem("like_"+$("#button_like span.icon_eye").attr("data-imgid")) == "1"){
                    return;
                  }
                  this.style.setProperty("color", "#fd5a42", "important");
                  $("#like_count").text(parseInt($("#like_count").text())+1);
                  $.ajax({
                    url: "",
                    type: "POST",
                    data: {like: $("#button_like span.icon_eye").attr("data-imgid")}
                  });
                  localStorage.setItem("like_"+$("#button_like span.icon_eye").attr("data-imgid"), "1");
                });
                $(".tags p a").on("click", function(){
                  localStorage.setItem("show_tag_filter", 1);
                  localStorage.setItem("show_left_menu", 1);
                });
            },
            afterShow: function(){
              $("#share_list_block").attr("data-url", document.location.href);
              $("#share_list_block").attr("data-title", fancyboxModels[this.href]["title"]);
              $("#share_list_block").socialLikes();
              $("#button_share").on("click", function(){
                  if($("#share_list").css("display") === "block")
                    {
                    $("#share_list").css("display", "none");
                    }
                    else
                  $("#share_list").css("display", "block");
                  //$("#over").css("display", "block");
                });
            },
            beforeClose: function() {
                window.location.hash = "";
                if(localStorage.getItem("show_tag_filter") != 1){
                  $(".white_sharing_button").css("display", "block");
                }
                $(".logo").css("display", "block");
            }
        });
        var hash = window.location.hash.replace("#", "");
        if (hash) {
            jQuery(".gallery a[data-hash=\"" + hash + "\"]").trigger("click");
        }
		(function(e,t,n,r){e.fn.lazy=function(r){function u(n){if(typeof n!="boolean")n=false;s.each(function(){var r=e(this);var s=this.tagName.toLowerCase();if(f(r)||n){if(r.attr(i.attribute)&&(s=="img"&&r.attr(i.attribute)!=r.attr("src")||s!="img"&&r.attr(i.attribute)!=r.css("background-image"))&&!r.data("loaded")&&(r.is(":visible")||!i.visibleOnly)){var u=e(new Image);++o;if(i.onError)u.error(function(){p(i.onError,r);h()});else u.error(function(){h()});var a=true;u.one("load",function(){var e=function(){if(a){t.setTimeout(e,100);return}r.hide();if(s=="img")r.attr("src",u.attr("src"));else r.css("background-image","url("+u.attr("src")+")");r[i.effect](i.effectTime);if(i.removeAttribute)r.removeAttr(i.attribute);p(i.afterLoad,r);u.unbind("load");u.remove();h()};e()});p(i.beforeLoad,r);u.attr("src",r.attr(i.attribute));p(i.onLoad,r);a=false;if(u.complete)u.load();r.data("loaded",true)}}});s=e(s).filter(function(){return!e(this).data("loaded")})}function a(){if(i.delay>=0)setTimeout(function(){u(true)},i.delay);if(i.delay<0||i.combined){u(false);e(i.appendScroll).bind("scroll",c(i.throttle,u));e(i.appendScroll).bind("resize",c(i.throttle,u))}}function f(e){var t=n.documentElement.scrollTop?n.documentElement.scrollTop:n.body.scrollTop;return t+l()+i.threshold>e.offset().top+e.height()}function l(){if(t.innerHeight)return t.innerHeight;if(n.documentElement&&n.documentElement.clientHeight)return n.documentElement.clientHeight;if(n.body&&n.body.clientHeight)return n.body.clientHeight;if(n.body&&n.body.offsetHeight)return n.body.offsetHeight;return i.fallbackHeight}function c(e,t){function s(){function o(){r=+(new Date);t.apply()}var s=+(new Date)-r;n&&clearTimeout(n);if(s>e||!i.enableThrottle)o();else n=setTimeout(o,e-s)}var n;var r=0;return s}function h(){--o;if(!s.size()&&!o)p(i.onFinishedAll,null)}function p(e,t){if(e){if(t!==null)e(t);else e()}}var i={bind:"load",threshold:500,fallbackHeight:2e3,visibleOnly:true,appendScroll:t,delay:-1,combined:false,attribute:"data-src",removeAttribute:true,effect:"show",effectTime:0,enableThrottle:false,throttle:250,beforeLoad:null,onLoad:null,afterLoad:null,onError:null,onFinishedAll:null};if(r)e.extend(i,r);var s=this;var o=0;if(i.bind=="load")e(t).load(a);else if(i.bind=="event")a();if(i.onError)s.bind("error",function(){p(i.onError,e(this))});return this};e.fn.Lazy=e.fn.lazy})(jQuery,window,document);
		jQuery("img.lazy").lazy({afterLoad: function(element) {
			element.removeClass("lazy");
		}});
		');

$this->registerJs("
  $(document).ready(function(){
    $('#ts_menu').css('height', ($('body').height()-350)+'px');
    $('#wrap_galleray_button_hide').css('height', ($('body').height())+'px');
    if(localStorage.getItem('show_left_menu') == null || localStorage.getItem('show_left_menu') == 0){
        $('#galleray_button_hide').addClass('show_b');
        $('#galleray_button').css('margin-left', '-30px');
        $('#galleray_button').css('opacity', '0.7');
        $('.galleray_main').css('padding-left', '30px');
        $('.galleray_left').hide();
        $('.fancybox-overlay-fixed').css('margin-left', '30px');
    }
    if(localStorage.getItem('show_tag_filter') == 1){
      $('#tags_sort').css('display', 'block');
      $('.white_sharing_button').css('display', 'none');
    }
    $('.white_sharing_button').on('click', function(){
      $('.white_sharing').css('display', 'block');
    });
    $('#white_sharing_close').on('click', function(){
      $('.white_sharing').css('display', 'none');
    });
    $('#galleray_button').on('click', function(){
      if($('#galleray_button_hide').hasClass('show_b')){
        localStorage.setItem('show_left_menu', 1);
        $('#galleray_button_hide').removeClass('show_b');
        $('.galleray_left').css('margin-left', '-230px');
        $('.galleray_left').show();
        $('#galleray_button').animate({'margin-left': \"-260px\"}, 700);
        $('#galleray_button').css('opacity', '1');
        $('.galleray_main').animate({'padding-left': '260px'}, 700);
        $('.fancybox-overlay-fixed').animate({'margin-left': '260px'}, 700, function(){
          $.fancybox.update();
        });
      }
      else{
        localStorage.setItem('show_left_menu', 0);
        $('#galleray_button_hide').addClass('show_b');
        $('#galleray_button').animate({'margin-left': '-30px'}, 700);
        $('#galleray_button').css('opacity', '0.7');
        $('.galleray_main').animate({'padding-left': '30px'}, 700, function(){
          $('.galleray_left').hide();
        });
        $('.fancybox-overlay-fixed').animate({'margin-left': '30px'}, 700, function(){
          $.fancybox.update();
        });
      }
    });
      $('#refresh_tags').on('click', function(){
        $.ajax({
                url: document.location.href,
                type: 'POST',
                data: {tags: 'tags'},
                success: function(html){
            $('#lag_main_list').html(html);
          }
        });
      });
      $('span.delete_tag').on('click', function(){
        $(this).parent().remove();
        var param = '';
        $('#tag_list a.del').each(function(){
          if($(this).text().length >2){
          param += $(this).text()+',';
          }

        });
        var loc = $('a.g_item.active').attr('href');
        loc = loc.substring(0, loc.indexOf('tag'));
        document.location.href = loc+'tag='+param;
      });
      $('#close_t').on('click', function(){
        $(this).parent().parent().remove();
        localStorage.setItem('show_tag_filter', 0);
        document.location.href = document.location.pathname;
      });

      $(document).on('scroll', function(){
      var sc = document.documentElement.scrollHeight - $('body').scrollTop()-$('body').height();
      var d =  (document.documentElement.scrollHeight - $('body').scrollTop())-$('body').height();
      var start = $('body').height();
      start = -150;
        if((sc)<(500) && document.documentElement.scrollHeight != $('body').height()){
          $('#galleray_left_menu').css('bottom', (-150+start*0.11+($('body').height()-d))+'px');
         // $('.white_sharing_button').css('bottom', (20+(start-d))+'px');
         var ttt = start*0.31+($('body').height()-d);
         if(($('body').height() - ttt)<100){
          ttt = $('body').height() - 100;
         }
          $('#galleray_button_hide').css('bottom', (ttt)+'px');
        }
        else{
          $('#galleray_left_menu').css('bottom', 'auto');
         // $('.white_sharing_button').css('bottom', '20px');
           $('#galleray_button_hide').css('bottom', ($('body').height())/2+'px');
        }
      });
  });
");
?>