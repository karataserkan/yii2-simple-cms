<?php
/**
 * Страница всех галлерей
 * @var yii\base\View $this
 * @var backend\modules\galleries\models\Gallery $models
 */

use yii\helpers\Html;
use yii\helpers\Json;
use frontend\modules\site\FancyBoxAsset;

$this->title = 'Галерея работ';
$this->params['pageClass'] = 'gallery';

$this->params['metaDescription'] = 'Моддинг и тюнинг iPhone — фотографии работ.';
$this->params['metaKeywords'] = 'Моддинг, тюнинг, iphone, 5s, 5, золото, porsche, дерево, кожа, крокодил, наппа, алькантара, Range Rover, Lamborghini, Ferrari';

$c = 0;
?>
<style>
	.lazy {opacity: 0;}
</style>
<?php if ($models) :
    $fancyboxModels = []; ?>
    <?php foreach ($models as $model) : ?>
        <h1 class="container-fluid"><?= $model['title'] ?></h1>
        <?php if ($model->galleries) : ?>
            <?php foreach ($model->galleries as $gallery) : ?>
                <div class="gallery">
                    <h2 class="container-fluid"><?= $gallery->title ?></h2>
                    <?php if ($gallery->images) : ?>
                        <?php foreach ($gallery->images as $image) :
                            $fancyboxModels[$image->url] = [
                                'title' => $image['title'],
                                'content' => $image['description'],
                                'tags' => ''
                            ]; ?>
                            <?php if ($image->tags) {
                                foreach ($image->tags as $key => $tag) {
                                    if ($key !== 0) {
                                        $fancyboxModels[$image->url]['tags'] .= ', ';
                                    }
                                    $fancyboxModels[$image->url]['tags'] .= Html::a('#' . $tag['name'], ['/search/default/index', 'tag' => $tag['name']]);
                                }
                            } ?>
                            <a href="<?= $image->url ?>" class="fancybox" rel="group" target="_blank" data-hash="<?= $image['id'] ?>">
                                <img <?= (++$c>8)?'class="lazy" data-':'' ?>src="<?= $image->thumb ?>" alt="<?= $image['title'] ?>" title="<?= $image['title'] ?>" />
                            </a>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>      
<?php endif;
    endforeach;

    FancyBoxAsset::register($this);
    $fancyboxModels = json_encode($fancyboxModels, JSON_UNESCAPED_UNICODE);
    $this->registerJs('var fancyboxModels = ' . $fancyboxModels . ';

        jQuery(".fancybox").fancybox({
            helpers: {
                title: {
                    type: "inside"
                }
            },
            padding: 0,
            margin: 0,
            afterLoad: function(current, previous) {
                var content = "<div class=\"fancybox-info\"><h1>" + fancyboxModels[this.href]["title"] + "</h1>";
                if (fancyboxModels[this.href]["content"]) {
                    content += fancyboxModels[this.href]["content"];
                }
                if (fancyboxModels[this.href]["tags"]) {
                    content += "<div class=\"tags\"><p>Тэги: " + fancyboxModels[this.href]["tags"] + "</p></div>";
                }
                content += "</div>";

                $.extend(this, {
                    aspectRatio: false,
                    type: "html",
                    width: "100%",
                    height: "100%",
                    content: "<div class=\"fancybox-image\" style=\"background-image:url(" + this.href + ");\">" + content + "</div>"
                });
            },
            beforeShow: function() {
                window.location.hash = this.element.data("hash");
            },
            beforeClose: function() {
                window.location.hash = "";
            }
        });
        var hash = window.location.hash.replace("#", "");
        if (hash) {
            jQuery(".gallery a[data-hash=\"" + hash + "\"]").trigger("click");
        }
		(function(e,t,n,r){e.fn.lazy=function(r){function u(n){if(typeof n!="boolean")n=false;s.each(function(){var r=e(this);var s=this.tagName.toLowerCase();if(f(r)||n){if(r.attr(i.attribute)&&(s=="img"&&r.attr(i.attribute)!=r.attr("src")||s!="img"&&r.attr(i.attribute)!=r.css("background-image"))&&!r.data("loaded")&&(r.is(":visible")||!i.visibleOnly)){var u=e(new Image);++o;if(i.onError)u.error(function(){p(i.onError,r);h()});else u.error(function(){h()});var a=true;u.one("load",function(){var e=function(){if(a){t.setTimeout(e,100);return}r.hide();if(s=="img")r.attr("src",u.attr("src"));else r.css("background-image","url("+u.attr("src")+")");r[i.effect](i.effectTime);if(i.removeAttribute)r.removeAttr(i.attribute);p(i.afterLoad,r);u.unbind("load");u.remove();h()};e()});p(i.beforeLoad,r);u.attr("src",r.attr(i.attribute));p(i.onLoad,r);a=false;if(u.complete)u.load();r.data("loaded",true)}}});s=e(s).filter(function(){return!e(this).data("loaded")})}function a(){if(i.delay>=0)setTimeout(function(){u(true)},i.delay);if(i.delay<0||i.combined){u(false);e(i.appendScroll).bind("scroll",c(i.throttle,u));e(i.appendScroll).bind("resize",c(i.throttle,u))}}function f(e){var t=n.documentElement.scrollTop?n.documentElement.scrollTop:n.body.scrollTop;return t+l()+i.threshold>e.offset().top+e.height()}function l(){if(t.innerHeight)return t.innerHeight;if(n.documentElement&&n.documentElement.clientHeight)return n.documentElement.clientHeight;if(n.body&&n.body.clientHeight)return n.body.clientHeight;if(n.body&&n.body.offsetHeight)return n.body.offsetHeight;return i.fallbackHeight}function c(e,t){function s(){function o(){r=+(new Date);t.apply()}var s=+(new Date)-r;n&&clearTimeout(n);if(s>e||!i.enableThrottle)o();else n=setTimeout(o,e-s)}var n;var r=0;return s}function h(){--o;if(!s.size()&&!o)p(i.onFinishedAll,null)}function p(e,t){if(e){if(t!==null)e(t);else e()}}var i={bind:"load",threshold:500,fallbackHeight:2e3,visibleOnly:true,appendScroll:t,delay:-1,combined:false,attribute:"data-src",removeAttribute:true,effect:"show",effectTime:0,enableThrottle:false,throttle:250,beforeLoad:null,onLoad:null,afterLoad:null,onError:null,onFinishedAll:null};if(r)e.extend(i,r);var s=this;var o=0;if(i.bind=="load")e(t).load(a);else if(i.bind=="event")a();if(i.onError)s.bind("error",function(){p(i.onError,e(this))});return this};e.fn.Lazy=e.fn.lazy})(jQuery,window,document);
		jQuery("img.lazy").lazy({afterLoad: function(element) { 
			element.removeClass("lazy"); 
		}});
		');
endif;