<?php
namespace frontend\modules\galleries\controllers;

use common\modules\galleries\models\Image;
use common\modules\tags\helpers\TagsHelper;
use common\modules\tags\models\Tag;
use common\modules\tags\models\TagModel;
use Yii;
use common\modules\galleries\modules\categories\models\Category;
use frontend\modules\site\components\Controller;
use yii\helpers\Url;

/**
 * Основной контроллер модуля [[Blogs]]
 */
class DefaultController extends Controller
{
  /**
   * @inheritdoc
   */
  public $layout = '/layout';

  public $enableCsrfValidation = false;

  /**
   * Страница всех моделей
   * @return mixed
   */
  public function actionIndex()
  {
    //ajax счётчик просмотров
    if(Yii::$app->request->post('id', false)){
      $im = Image::findOne(Yii::$app->request->post('id', 0));
      $im->views +=1;
      $im->update(false);
      Yii::$app->end();
    }
    //ajax like
    if(Yii::$app->request->post('like', false)){
      $im = Image::findOne(Yii::$app->request->post('like', 0));
      $im->like +=1;
      $im->update(false);
      Yii::$app->end();
    }
    //ajax tags
    if(Yii::$app->request->post('tags', false)){
      return $this->renderPartial(
        'index_ajax',
        [
          'query' => Url::toRoute(['', 'id' => Yii::$app->request->get('id', ""), 'tag' => Yii::$app->request->get('tag', "")]),
          'tag_list' => Tag::find()->leftJoin('ug4k_tag_model', 'ug4k_tag_model.tag_id = ug4k_tags.id')->where(['model_class_id'=>TagsHelper::crcClassName(Image::className())])->orderBy('RAND()')->limit(5)->all(),
        ]
      );
    }
    //если есть теги
    $tags_array = array();
    $array_img = array();
    if(Yii::$app->request->get('tag', false) && Yii::$app->request->get('tag', false) != ""){
      $array_search = explode(",", trim(Yii::$app->request->get('tag', ""), ','));
      foreach($array_search as $k=>$t){
        if(strlen($t)<2)
          unset($array_search[$k]);
      }
      $tags = Tag::find()->with('models')->where(["in", "name", $array_search])->all();
      foreach($tags as $tag){
        $tags_array[$tag->id] = $tag->name;
        $array_img[] = $tag->id;
      }
    }

    if(count($array_img)>0){
      $query = Image::find()
        ->with('gallery')
        ->leftJoin('ug4k_galleries', 'ug4k_gallery_image.gallery_id = ug4k_galleries.id')
        ->leftJoin('ug4k_galleries_categories', 'ug4k_galleries.category_id = ug4k_galleries_categories.id')
        ->leftJoin('ug4k_tag_model', 'ug4k_gallery_image.id = ug4k_tag_model.model_id')
        ->where(['in', 'ug4k_tag_model.tag_id', $array_img])->andWhere(['model_class_id'=>TagsHelper::crcClassName(Image::className())]);
    }
    else{
      $query = Image::find()
        ->with('gallery')
        ->leftJoin('ug4k_galleries', 'ug4k_gallery_image.gallery_id = ug4k_galleries.id')
        ->leftJoin('ug4k_galleries_categories', 'ug4k_galleries.category_id = ug4k_galleries_categories.id');
    }
    $cm = $query->all();
    if (Yii::$app->request->get('id', false) && Yii::$app->request->get('id', false) != "") {
      $models = $query->andWhere(['ug4k_galleries_categories.id' => Yii::$app->request->get('id', 0)])->orderBy('ug4k_galleries.ordering, ug4k_gallery_image.create_time DESC')->all();
    }
    else{
      $models = $query->orderBy('ug4k_galleries.ordering, ug4k_gallery_image.create_time DESC')->all();
    }
    $amount = 0;
    $count_img = array();

    foreach($cm as $model){
      if(!isset($count_img[$model->gallery->category->id])){
        $count_img[$model->gallery->category->id] = 0;
      }
      $count_img[$model->gallery->category->id] +=1;
    }

    $amount = array_sum($count_img);
    return $this->render(
      'index_new',
      [
        'models' => $models,
        'count_img' => $count_img,
        'category' => Category::find()->with('galleries')->published()->orderBy('ordering ASC')->all(),
        'category_id' => Yii::$app->request->get('id', false),
        'amount_img' => $amount,
        'tags' => $tags_array,
        'tag_list' => Tag::find()->leftJoin('ug4k_tag_model', 'ug4k_tag_model.tag_id = ug4k_tags.id')->where(['model_class_id'=>TagsHelper::crcClassName(Image::className())])->orderBy('RAND()')->limit(5)->all(),
      ]
    );
  }

  public function actionAjaxTags(){
    return $this->render(
      'index_ajax',
      [
        'query' => Yii::$app->request->post('query', ""),
        'tag_list' => Tag::find()->with('models')->orderBy('RAND()')->limit(5)->all(),
      ]
    );
  }

  public function actionAjaxLoad(){
      $models = Image::find()->all();
      return $this->render(
        'index_ajax',
        [
          'models' => $models
        ]
      );
  }

  /**
   * Страница всех моделей
   * @return mixed
   */
  public function actionOld()
  {
    $models = Category::find()->with('galleries')->published()->orderBy('ordering ASC')->all();

    return $this->render(
      'index',
      [
        'models' => $models,
      ]
    );
  }

}