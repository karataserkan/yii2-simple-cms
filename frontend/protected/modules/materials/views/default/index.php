<?php
/**
 * Страница всех блогов
 * @var yii\base\View $this
 * @var common\modules\blogs\models\Post $dataProvider
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\widgets\ListView;
use frontend\modules\site\FancyBoxAsset;

$this->title = 'Материалы, приминяемые для производства эксклюзивных и персонализированных iPhone';
$this->params['pageClass'] = 'materials';
$this->params['metaDescription'] = 'Карта материалов, используемых при создании эксклюзивных, персонализированных iPhone. Описания и свойства.';
$this->params['metaKeywords'] = 'Кожа, нильский крокодил, наппа, white bentley, montana, текстурированная кожа, lambroghini, клен, птичий глаз, дерево, камфора, черное дерево, цейлонский эбен, карельская береза, перламутр, карбон, сухой карбон, золото, эмаль, титан, серебро';
?>
<h1 class="container-fluid bordered">Материалы — свойства и описания</h1>
<?php if ($models) :
	$fancyboxModels = []; ?>
	<div class="materials-list">
		<?php foreach ($models as $model) :
			$fancyboxModels[$model->image] = [
				'title' => $model['title'],
				'content' => $model['content'],
				'tags' => '',
				'list' => ''
			];
			if ($model->tags) {
				foreach ($model->tags as $key => $tag) {
					if ($key !== 0) {
						$fancyboxModels[$model->image]['tags'] .= ', ';
					}
					$fancyboxModels[$model->image]['tags'] .= Html::a('#' . $tag['name'], ['/search/default/index', 'tag' => $tag['name']]);
				}
			}
			if ($model->children) {
				foreach ($model->children as $key => $child) {
					$fancyboxModels[$child->image] = [
						'title' => $child['title'],
						'content' => $child['content'],
						'tags' => ''
					];
					if ($key !== 0) {
						$fancyboxModels[$model->image]['list'] .= ', ';
					}
					$fancyboxModels[$model->image]['list'] .= Html::a($child['color'], $child->image, ['class' => 'fancybox', 'rel' => 'group-' . $child['parent_id'], 'target' => '_blank', 'data-id' => $child['id']]);

					if ($child->tags) {
						foreach ($child->tags as $index => $tag) {
							if ($index !== 0) {
								$fancyboxModels[$child->image]['tags'] .= ', ';
							}
							$fancyboxModels[$child->image]['tags'] .= Html::a($tag['name'], ['/search/default/index', 'tag' => $tag['name']]);
						}
					}
				}
			}
		?>
			<a href="<?= $model->image ?>" data-hash="<?= $model['id'] ?>" class="fancybox" rel="group" target="_blank">
				<img src="<?= $model->thumb ?>" alt="<?= $model['title'] ?>" title="<?= $model['title'] ?>" />
			</a>
		<?php endforeach; ?>
	</div>

	<?php FancyBoxAsset::register($this);
	$fancyboxModels = Json::encode($fancyboxModels);
	$this->registerJs('var fancyboxModels = ' . $fancyboxModels . ';
		jQuery(".fancybox").fancybox({
	    helpers: {
	        title: {
	            type: "inside"
	        }
	    },
	    padding: 0,
	    margin: 0,
	    afterLoad: function(current, previous) {
	    	var content = "<div class=\"fancybox-info\"><h1>" + fancyboxModels[this.href]["title"] + "</h1>";
	    	if (fancyboxModels[this.href]["content"]) {
	    		content += fancyboxModels[this.href]["content"];
	    	}
	    	if (fancyboxModels[this.href]["list"]) {
	    		content += "<div class=\"colors\"><p>Дополнительные изображения: " + fancyboxModels[this.href]["list"] + "</p></div>";
	    	}
	    	if (fancyboxModels[this.href]["tags"]) {
	    		content += "<div class=\"tags\"><p>Тэги: " + fancyboxModels[this.href]["tags"] + "</p></div>";
	    	}
	    	content += "</div>";

	        $.extend(this, {
	            aspectRatio: false,
	            type: "html",
	            width: "100%",
	            height: "100%",
	            content: "<div class=\"fancybox-image\" style=\"background-image:url(" + this.href + ");\">" + content + "</div>"
	        });
	    },
	    beforeShow: function() {
	    	window.location.hash = this.element.data("hash");
	    },
	    beforeClose: function() {
	        window.location.hash = "";
	    }
	});
	var hash = window.location.hash.replace("#", "");
	if (hash) {
	    jQuery(".materials-list a[data-hash=\"" + hash + "\"]").trigger("click");
	}');
endif; ?>