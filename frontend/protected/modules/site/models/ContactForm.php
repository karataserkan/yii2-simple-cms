<?php
namespace frontend\modules\site\models;

use Yii;
use yii\base\Model;

/**
 * Class ContactForm
 * @package frontend\modules\site\models
 * Модель обратной связи
 *
 * @property string $name Имя
 * @property string $email E-mail
 * @property string $body Сообщение
 * @property string $verifyCode Код верификации
 */
class ContactForm extends Model
{
	public $name;
	public $surname;
	public $email;
	public $phone;
	public $body;
	public $verifyCode;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['email', 'body', 'phone'], 'filter', 'filter' => 'trim'],

			// [[name]] и [[email]] обязательны к заполнению
			[['name', 'email'], 'required'],

			// [[email]] должен быть валидным E-mail
			['email', 'email'],

			// [[phone]] телефон
			// ['phone', 'match', 'pattern' => '/[0-9]+/'],

			// [[verifyCode]] должен быть введен правильно
			['verifyCode', 'captcha', 'captchaAction' => 'site/default/captcha']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
		    'name' => 'Имя',
		    'surname' => 'Фамилия',
		    'email' => 'Адрес электронной почты',
		    'phone' => 'телефон',
		    'body' => 'Ваш вопрос',
			'verifyCode' => 'Код верификации',
		];
	}

	/**
	 * Отправляем собраную ифнормацию с помощью текущей модели на указаный E-mail адрес.
     * @param string $email E-mail адрес на который будет обправлено сообщение
     * @return boolean Статус валидации модели
     */
    public function contact($email)
    {
    	if ($this->validate()) {
    		Yii::$app->mail
    		         ->compose('site/feedback-admin', [
                        'surname' => $this->surname,
                        'name' => $this->name,
                        'email' => $this->email,
                        'adminEmail' => $email,
    		         	'phone' => $this->phone,
    		         	'body' => $this->body
    		         ])
    		         ->setFrom([$this->email => $this->name . $this->surname])
    		         ->setTo($email)
    		         ->setSubject('Вопрос на сайте ЭтоСервис')
    		         ->send();

            Yii::$app->mail
                     ->compose('site/feedback-user', [
                        'surname' => $this->surname,
                        'name' => $this->name,
                        'email' => $this->email
                     ])
                     ->setFrom($email)
                     ->setTo($this->email)
                     ->setSubject('Вопрос на сайте ЭтоСервис')
                     ->send();

            return true;
        } else {
        	return false;
        }
    }
}