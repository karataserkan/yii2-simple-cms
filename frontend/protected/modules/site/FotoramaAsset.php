<?php
namespace frontend\modules\site;

use yii\web\AssetBundle;

class FotoramaAsset extends AssetBundle
{
	public $sourcePath = '@frontend/modules/site/assets';
	public $css = [
		 'css/fotorama.css'
	];
    public $js = [
        'js/fotorama.js',
    ];
    public $depends = [
        'frontend\modules\site\AppAsset',
        'yii\web\JqueryAsset'
    ];
}