<?php
/**
 * Created by PhpStorm.
 * User: arseniy
 * Date: 08/10/14
 * Time: 23:03
 */

namespace frontend\modules\site;
use yii\web\AssetBundle;

class BlogsAsset extends AssetBundle
{
  public $sourcePath = '@frontend/modules/site/assets';
  public $js = [
    'js/lastPostAjax.js',
  ];
  public $depends = [
    'frontend\modules\site\AppAsset',
    'yii\web\JqueryAsset'
  ];
} 