<?php
namespace frontend\modules\blogs\widgets\blocks4;

use yii\base\Widget;
use common\modules\blogs\modules\blocks\models\Blocks;

/**
 * Виджет [[News]]
 * Выводит последние или популярные новости в зависимости от настроек
 * @var yii\base\Widget $this
 */
class Blocks4 extends Widget
{
	/**
	 * @var string Заголовок виджета
	 */
	public $title;

	/**
	 * @var string Заголовок виджета 2-го урованя
	 */
	public $secondaryTitle;

	/**
	 * @var boolean Определяем если нужно показывать ссылку на все посты
	 */
	public $all = true;

	/**
	 * @var boolean В случае false будут выводится самые популярные посты
	 * По умолчанею выводятся последние посты
	 */
	public $recent = true;

    /**
     * @var int ИД категории
     */
    public $category = 1;

	public function run()
	{

		  $models = Blocks::find()->limit(4)->all();

    	return $this->render('index', ['models' => $models, 'secondaryTitle'=>$this->secondaryTitle]);
  	}
}