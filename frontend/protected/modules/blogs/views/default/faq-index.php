<?php
/**
 * Страница всех блогов
 * @var yii\base\View $this
 * @var common\modules\blogs\models\Post $dataProvider
 */

use yii\helpers\Html;
use yii\widgets\ListView;
use frontend\modules\blogs\widgets\lastpost\LastPost;

$this->title = 'Часто задаваемые вопросы об iPhone и iPad';
$this->params['pageClass'] = 'blog';
?>
<div id="LastPost">
  <?= LastPost::widget([
      'secondaryTitle' => 'Интересное',
      'category' => 2
    ]); ?>
</div>