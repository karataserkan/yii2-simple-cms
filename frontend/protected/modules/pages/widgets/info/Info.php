<?php
namespace frontend\modules\pages\widgets\info;

use yii\base\Widget;
use yii\helpers\Html;
use common\modules\pages\models\Page;

/**
 * Виджет [[Services]]
 * Список услуг на главной странице.
 * @var yii\base\Widget $this Виджет
 * 
 * Пример использования:
 * ~~~
 * echo Services::widget();
 * ~~~
 */
class Info extends Widget
{
	/**
	 * @inheritdoc
	 */
	public function run()
	{
		$models = Page::find()->info()->with('parent')->all();
		return $this->render('index', [
			'models' => $models
		]);
  	}
}