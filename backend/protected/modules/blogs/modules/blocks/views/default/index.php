<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

 $form = ActiveForm::begin([
    'id' => 'blocks-form',
    'options' => ['class' => 'form-horizontal'],
  ]) ?>
<h2>Настройка блоков</h2>
<table style="width: 100%">
  <tr>
    <td style="width: 30%; padding-right: 40px;">
      <?= $form->field($model, 'title_button1') ?>
      <?= $form->field($model, 'title_button2') ?>
      <?= $form->field($model, 'title_button3') ?>
      <?= $form->field($model, 'title_button4') ?>
    </td>
    <td style="width: 10px;"></td>
    <td style="width: 30%; padding-right: 40px;">
      <?= $form->field($model, 'title1') ?>
      <?= $form->field($model, 'title2') ?>
      <?= $form->field($model, 'title3') ?>
      <?= $form->field($model, 'title4') ?>
    </td>
    <td style="width: 10px;"></td>
    <td style="width: 30%; padding-right: 40px;">
      <?= $form->field($model, 'link1') ?>
      <?= $form->field($model, 'link2') ?>
      <?= $form->field($model, 'link3') ?>
      <?= $form->field($model, 'link4') ?>
    </td>
  </tr>
</table>


<div class="form-group">
  <div class="col-lg-offset-1 col-lg-11">
    <?= Html::submitButton('Обновить', ['class' => 'btn btn-primary']) ?>
  </div>
</div>
<?php ActiveForm::end() ?>

<script>
  setTimeout('ineer()', 2000);
  function ineer(){
    $('#blocksform-title_button3').keyup(function(){
      var l = 50 - $('#blocksform-title_button3').val().length;
      $('#blocksform-title_button3').parent().find('.help-block').text(l);
      if(l < 0){
        $('#blocksform-title_button3').parent().find('.help-block').css('color', '#c00e0e');
      }
      else{
        $('#blocksform-title_button3').parent().find('.help-block').css('color', '');
      }
    });
    $('#blocksform-title_button1').keyup(function(){
      var l = 50 - $('#blocksform-title_button1').val().length;
      $('#blocksform-title_button1').parent().find('.help-block').text(l);
      if(l < 0){
        $('#blocksform-title_button1').parent().find('.help-block').css('color', '#c00e0e');
      }
      else{
        $('#blocksform-title_button1').parent().find('.help-block').css('color', '');
      }
    });
      $('#blocksform-title_button2').keyup(function(){
        var l = 50 - $('#blocksform-title_button2').val().length;
        $('#blocksform-title_button2').parent().find('.help-block').text(l);
        if(l < 0){
          $('#blocksform-title_button2').parent().find('.help-block').css('color', '#c00e0e');
        }
        else{
          $('#blocksform-title_button2').parent().find('.help-block').css('color', '');
        }
      });
    $('#blocksform-title_button4').keyup(function(){
      var l = 50 - $('#blocksform-title_button4').val().length;
      $('#blocksform-title_button4').parent().find('.help-block').text(l);
      if(l < 0){
        $('#blocksform-title_button4').parent().find('.help-block').css('color', '#c00e0e');
      }
      else{
        $('#blocksform-title_button4').parent().find('.help-block').css('color', '');
      }
    });
  }
</script>