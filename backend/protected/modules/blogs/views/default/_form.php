<?php
/**
 * Представление формы поста.
 * @var yii\web\View $this Представление
 * @var yii\widgets\ActiveForm $form Форма
 * @var common\modules\blogs\models\Post $model Модель
 */

use backend\modules\seo\widgets\seo\Seo;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\jui\DatePicker;
use common\extensions\imperavi\Imperavi;
use common\extensions\fileapi\FileAPIAdvanced;
use backend\modules\tags\widgets\tags\Tags;

$form = ActiveForm::begin([
	'enableClientValidation' => false,
	'enableAjaxValidation' => true,
	'validateOnChange' => false
]); ?>
    <div class="row">
        <div class="col-sm-12">
            <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Обновить', [
                'class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right'
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'title') .
                $form->field($model, 'alias') .
                $form->field($model, 'author_id')->dropDownList($userArray, [
                    'prompt' => 'Выберите автора'
                ]) .
                $form->field($model, 'status_id')->dropDownList($statusArray, [
                    'prompt' => 'Выберите статус'
                ]) .
                $form->field($model, 'categoryIds')->listBox($categoryArray, ['multiple' => true, 'unselect' => '']) .
                $form->field($model, 'createTimeJui')->widget(DatePicker::className(), [
                    'options' => [
                        'class' => 'form-control'
                    ],
                    'clientOptions' => [
                        'dateFormat' => 'yy-mm-dd',
                        'changeMonth' => true,
                        'changeYear' => true
                    ]
                ]);
            ?>
            <?= Seo::widget([
                'form' => $form,
                'model' => $model,
                'shortForm' => true
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'image_url')->widget(FileAPIAdvanced::className(), [
                'url' => $this->context->module->imageUrl(),
                'deleteUrl' => Url::toRoute('/blogs/default/delete-image'),
                'deleteTempUrl' => Url::toRoute('/blogs/default/deleteTempImage'),
                'settings' => [
                    'url' => Url::toRoute('uploadTempImage'),
                    'imageTransform' => [
                        'imageOriginal' => false,
                        'maxWidth' => $this->context->module->imageWidth
                    ]
                ]
             ]); ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'preview_url')->widget(FileAPIAdvanced::className(), [
                'url' => $this->context->module->previewUrl(),
                'deleteUrl' => Url::toRoute('/blogs/default/delete-preview'),
                'deleteTempUrl' => Url::toRoute('/blogs/default/deleteTempPreview'),
                'settings' => [
                    'url' => Url::toRoute('uploadTempPreview'),
                    'imageTransform' => [
                        'imageOriginal' => false,
                        'width' => $this->context->module->previewWidth,
                        'height' => $this->context->module->previewHeight,
                        'preview' => true
                    ]
                ]
             ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'tagArray')->widget(Tags::className(), [
                'settings' => [
                    'width' => '100%'
                ]
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'snippet')->textarea() .
                $form->field($model, 'content')->widget(Imperavi::className(), [
                    'settings' => [
                        'imageUpload' => Url::toRoute('/blogs/default/imperavi-upload'),
                        'imageGetJson' => Url::toRoute('/blogs/default/imperavi-get'),
                    ]
                ]) .
                Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Обновить', [
                    'class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right'
                ]); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php echo Yii::$app->controller->renderPartial('@backend/modules/pages/views/default/gallery/form', ['pageAlias' => $model->alias]); ?>