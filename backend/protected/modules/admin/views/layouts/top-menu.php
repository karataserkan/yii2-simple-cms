<?php
/**
 * Верхнее меню backend-приложения.
 * @var yii\base\View $this Предсталвение
 * @var array $params Основные параметры представления
 */

use yii\bootstrap\Nav;

$leftItems = [
    [
        'label' => 'Пользователи',
        'url' => ['/users/default/index']
    ],
    [
        'label' => 'Блоги',
        'url' => ['/blogs/default/index'],
        'items' => [
            [
                'label' => 'Посты',
                'url' => ['/blogs/default/index']
            ],
            [
                'label' => 'Категории',
                'url' => ['/blogs/categories/default/index']
            ],
            [
                'label' => 'Настройка блоков',
                'url' => ['/blogs/blocks/default/index']
            ]
        ]
    ],
    [
        'label' => 'Статические страницы',
        'url' => ['/pages/default/index']
    ],
    [
        'label' => 'Галереи',
        'url' => ['/galleries/default/index'],
        'items' => [
            [
                'label' => 'Галереи',
                'url' => ['/galleries/default/index']
            ],
            [
                'label' => 'Категории',
                'url' => ['/galleries/categories/default/index']
            ],
            [
                'label' => 'Последняя работа',
                'url' => ['/galleries/images/last']
            ]
        ]
    ],
    [
        'label' => 'Меню',
        'url' => ['/menu/default/index'],
        'items' => [
            [
                'label' => 'Меню',
                'url' => ['/menu/default/index']
            ],
            [
                'label' => 'Пунткы меню',
                'url' => ['/menu/items/index']
            ]
        ]
    ],
    [
        'label' => 'Тэги',
        'url' => ['/tags/default/index']
    ],
    [
        'label' => 'Материалы',
        'url' => ['/materials/default/index']
    ],
    [
        'label' => 'Слайдер',
        'url' => ['/slider/default/index']
    ],
];

$rightItems = [
    [
        'label' => 'Перейти на сайт',
        'url' => Yii::$app->params['siteDomain']
    ]
];
if (!Yii::$app->user->isGuest) {
    $rightItems[] = [
        'label' => 'Выход',
        'url' => ['/users/default/logout']
    ];
}

echo Nav::widget([
    'id' => 'topmenu',
    'options' => ['class' => 'navbar-nav navbar-left'],
    'items' => $leftItems
]);

echo Nav::widget([
    'id' => 'topmenu',
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => $rightItems
]);