<?php
/**
 * Представление главной старницы панели управления.
 * @var yii\base\View $this
 */

use yii\helpers\Html;

use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use common\modules\blogs\models\Post;

$this->title = 'Панель управления'; ?>
<div class="jumbotron text-center">
  <h1><?php echo Html::encode($this->title); ?></h1>
  <p>В будущем тут возможно будут добавлены удобные фичи.</p>
</div>