<?php

namespace backend\modules\admin\grid;

use Closure;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\Column;

/**
 * To add a CheckboxColumn to the [[GridView]], add it to the [[GridView::columns|columns]] configuration as follows:
 *
 * ```php
 * 'columns' => [
 *     [
 *         'class' => 'backend\modules\admin\grid\OrderingColumn',
 *         // you may configure additional properties here
 *     ],
 * ]
 * ```
 */
class OrderingColumn extends Column
{
    /**
     * @var string the name of the input checkbox input fields. This will be appended with `[]` to ensure it is an array.
     */
    public $name = 'ordering';
    /**
     * @var string the attribute name associated with this column. When neither [[content]] nor [[value]]
     * is specified, the value of the specified attribute will be retrieved from each data model and displayed.
     *
     * Also, if [[label]] is not specified, the label associated with the attribute will be displayed.
     */
    public $attribute;
    /**
     * @var string|array Метод к которому нужно оправить запрос сохранения элемента.
     */
    public $action = 'ordering';
    /**
     * @var array HTML attributes for the checkboxes.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $inputOptions = [];

    /**
     * @var string The replacement pattern for model primary key.
     */
    private $_primaryKeyReplacement = '{%primaryKey}';

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException if [[name]] is not set.
     */
    public function init()
    {
        parent::init();

        if (empty($this->attribute)) {
            throw new InvalidConfigException('The "attribute" property must be set.');
        }
        if (empty($this->name)) {
            throw new InvalidConfigException('The "name" property must be set.');
        } else {
            $this->name .= '[' . $this->_primaryKeyReplacement . ']';
        }
    }

    /**
     * @inheritdoc
     */
    protected function renderHeaderCellContent()
    {
        $name = rtrim($this->name, '[' . $this->_primaryKeyReplacement . ']') . '-save';

        if ($this->header !== null) {
            return parent::renderHeaderCellContent();
        } else {
            $this->registerClientScript();
            return Html::a('<span class="glyphicon glyphicon-floppy-disk"></span>', '#', ['class' => $name]);
        }
    }

    /**
     * @inheritdoc
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        if ($this->inputOptions instanceof Closure) {
            $options = call_user_func($this->inputOptions, $model, $key, $index, $this);
        } else {
            $options = $this->inputOptions;
            $value = ArrayHelper::getValue($model, $this->attribute);
            if (isset($options['value'])) {
                $value = $options['value'];
                unset($options['value']);
            }
            if (!isset($options['class'])) {
                $options['class'] = 'form-control';
            }
        }

        $name = str_replace($this->_primaryKeyReplacement, $model->primaryKey, $this->name);

        $output = '<div class="row"><div class="col-xs-10">' .
                  Html::textInput($name, $value, $options) .
                  '</div><div class="col-xs-2">' .
                  Html::a('<span class="glyphicon glyphicon-arrow-up ordering-up"></span>', '#') .
                  Html::a('<span class="glyphicon glyphicon-arrow-down ordering-down"></span>', '#') .
                  '</div></div>';

        return $output;
    }

    public function registerClientScript()
    {
        $id = $this->grid->options['id'];
        $name = rtrim($this->name, '[' . $this->_primaryKeyReplacement . ']');
        $save = $name . '-save';
        $action = Url::toRoute($this->action);

        $this->grid->getView()->registerJs("jQuery(document).on('click', '#$id .$save', function(evt) {
            evt.preventDefault();

            var el = jQuery(this);
            jQuery.ajax({
                url: '$action',
                type: 'POST',
                data: jQuery('#$id input[name^=\'$name\']').serialize(),
                beforeSend: function(jqXHR, settings) {
                    el.css('opacity', '0.2');
                },
                complete: function(jqXHR, textStatus) {
                    el.css('opacity', '1');
                }
            });
        });
        jQuery(document).on('click', '#$id .ordering-up, #$id .ordering-down', function(evt) {
            evt.preventDefault();
            
            var el = jQuery(this),
                input = el.parents('.row').find('input'),
                inputVal = input.val();

            if (el.hasClass('ordering-up')) {
                inputVal = parseInt(inputVal) + 1;
            } else if (el.hasClass('ordering-down') && inputVal > 0) {
                inputVal = parseInt(inputVal) - 1;
            }

            input.val(inputVal);

            jQuery.ajax({
                url: '$action',
                type: 'POST',
                data: input.serialize(),
                beforeSend: function(jqXHR, settings) {
                    el.css('opacity', '0.2');
                },
                complete: function(jqXHR, textStatus) {
                    el.css('opacity', '1');
                }                
            });

        })");
    }
}
