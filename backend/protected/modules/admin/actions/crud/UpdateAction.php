<?php
namespace backend\modules\admin\actions\crud;

use common\modules\seo\models\Seo;
use yii\base\InvalidConfigException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use Yii;

/**
 * UpdateAction экшен обновления существующей записи модели.
 *
 * Пример использования:
 * ~~~
 * public function actions()
 * {
 *     'update' => [
 *         'class' => UpdateAction::className(),
 *         'model' => Post::className(),
 *         'view' => 'update',
 *         'scenario' => 'admin-update'
 *     ]
 * }
 * ~~~
 */
class UpdateAction extends Action
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if ($this->view === null) {
            throw new InvalidConfigException('{$this->view} is required.');
        }
    }

    /**
     * @inheritdoc
     */
    public function run($id)
    {
        $model = $this->findModel($id);
        if ($this->scenario !== null) {
            $model->setScenario($this->scenario);
        }

        if ($this->seo) {
            $seoModel = ($model->seo === null) ? new Seo() : $model->seo;
        }

        if ($model->load(Yii::$app->getRequest()->post()) && $model->save(false)) {
            if ($this->seo && $seoModel->load(Yii::$app->getRequest()->post())) {
                if ($seoModel->isNewRecord) {
                    $seoModel->model_id = $model->id;
                    $seoModel->model_class_id = $model->getModelClassId();
                }
                $seoModel->save(false);
            }
            return $this->controller->refresh();
        } elseif (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        } else {
            $this->params['model'] = $model;
            return $this->controller->render($this->view, $this->params);
        }
    }
}
