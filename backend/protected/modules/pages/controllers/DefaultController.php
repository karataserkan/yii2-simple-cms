<?php
namespace backend\modules\pages\controllers;

use app\helpers\AppAsset;
use Yii;
use yii\web\Response;
use yii\web\HttpException;
use yii\web\View;
use yii\widgets\ActiveForm;
use common\modules\seo\models\Seo;
use common\modules\pages\models\Page;
use backend\modules\pages\models\search\PageSearch;
use backend\modules\admin\components\Controller;
use backend\modules\admin\actions\crud\IndexAction;
use backend\modules\admin\actions\crud\ViewAction;
use backend\modules\admin\actions\crud\CreateAction;
use backend\modules\admin\actions\crud\UpdateAction;
use backend\modules\admin\actions\crud\DeleteAction;
use backend\modules\admin\actions\crud\BatchDeleteAction;
use common\extensions\fileapi\actions\UploadAction as FileAPIUploadAction;
use common\extensions\fileapi\actions\DeleteAction as FileAPIDeleteAction;
use common\extensions\imperavi\actions\UploadAction as ImperaviUploadAction;
use common\extensions\imperavi\actions\GetAction as ImperaviGetAction;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use yii\web\UploadedFile;
use yii\helpers\Url;
use yii\helpers\Json;

/**
 * Основной контроллер модуля Blogs
 */
class DefaultController extends Controller
{
    /**
     * @return string Класс основной модели контролера.
     */
    public function getModelClass()
    {
        return Page::className();
    }

    /**
     * @return string Класс основной поисковой модели контролера.
     */
    public function getSearchModelClass()
    {
        return PageSearch::className();
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::className(),
                'model' => $this->modelClass,
                'searchModel' => $this->searchModelClass,
                'view' => 'index',
                'params' => [
                    'statusArray' => Page::getStatusArray()
                ]
            ],
            'view' => [
                'class' => ViewAction::className(),
                'model' => $this->modelClass,
                'view' => 'view'
            ],
            'delete' => [
                'class' => DeleteAction::className(),
                'model' => $this->modelClass
            ],
            'batch-delete' => [
                'class' => BatchDeleteAction::className(),
                'model' => $this->modelClass
            ],
            'upload-temp-image' => [
                'class' => FileAPIUploadAction::className(),
                'path' => $this->module->imageTempPath(),
                'types' => $this->module->imageAllowedExtensions,
                'minWidth' => $this->module->imageWidth
            ],
            'delete-temp-image' => [
                'class' => FileAPIDeleteAction::className(),
                'path' => $this->module->imageTempPath()
            ],
            'imperavi-upload' => [
                'class' => ImperaviUploadAction::className(),
                'path' => $this->module->contentPath(),
                'pathUrl' => $this->module->contentUrl()
            ],
            'imperavi-get' => [
                'class' => ImperaviGetAction::className(),
                'path' => $this->module->contentPath(),
                'pathUrl' => $this->module->contentUrl()
            ]
        ];
    }

    /**
     * Создаем новую модель.
     * В случае успеха, пользователь будет перенаправлен на view страницу
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Page(['scenario' => 'admin-create']);
        $seoModel = new Seo;
        $statusArray = Page::getStatusArray();
        $parentsArray = $model->getParentsArray();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->getRequest()->post()) && $seoModel->load(Yii::$app->getRequest()->post()) && $model->validate() && $seoModel->validate()) {
                if ($model->save(false)) {
                    $seoModel->model_id = $model->id;
                    $seoModel->model_class_id = $model->getModelClassId();
                    if ($seoModel->save(false)) {
                        return $this->redirect(['update', 'id' => $model['id']]);
                    }
                }
            } else {
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return array_merge(ActiveForm::validate($model), ActiveForm::validate($seoModel));
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'statusArray' => $statusArray,
            'parentsArray' => $parentsArray
        ]);
    }

    /**
     * Обновляем модель
     * В случае успеха, пользователь будет перенаправлен на view страницу
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->setScenario('admin-update');
        $seoModel = ($model->seo === null) ? new Seo() : $model->seo;
        $statusArray = Page::getStatusArray();
        $parentsArray = $model->getParentsArray();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->getRequest()->post()) && $seoModel->load(Yii::$app->getRequest()->post()) && $model->validate() && $seoModel->validate()) {
                if ($model->save(false)) {
                    if ($seoModel->isNewRecord) {
                        $seoModel->model_id = $model->id;
                        $seoModel->model_class_id = $model->getModelClassId();
                    }
                    if ($seoModel->save(false)) {
                        return $this->refresh();
                    }
                }
            } else {
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return array_merge(ActiveForm::validate($model), ActiveForm::validate($seoModel));
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'statusArray' => $statusArray,
            'parentsArray' => $parentsArray
        ]);
    }

    /**
     * Удаляем изображение поста.
     */
    function actionDeleteImage()
    {
        if ($id = Yii::$app->request->getBodyParam('id')) {
            $model = $this->findModel($id);
            $model->setScenario('delete-image');
            $model->save(false);
        } else {
            throw new HttpException(400);
        }
    }

    /**
     * Поиск модели по первичному ключу.
     * Если модель не найдена, будет вызвана 404 ошибка.
     * @param integer|array $id Идентификатор модели.
     * @return ActiveRecord Найденая модель.
     * @throws HttpException с кодом 404, если модель не найдена.
     */
    protected function findModel($id)
    {
        $ar = $this->getModelClass();

        if (is_array($id)) {
            $model = $ar::findIdentities($id);
        } else {
            $model = $ar::findIdentity($id);
        }

        if ($model !== null) {
            return $model;
        } else {
            throw new HttpException(404);
        }
    }

    /**
     * Генерация кода галлереи
     */
    public function actionGenerateGalleryCode()
    {
        $response = ['success' => true, 'message' => '', 'galleryCode' => ''];
        $request = Yii::$app->request;

        if (!$request->post('gallery-image-big')) {
            $response['success'] = false;
            $response['message'] = 'Нужно добавить мин. 1 изображение';
            echo Json::encode($response);
            Yii::$app->end();
        }

        $imagesCount = count($request->post('gallery-image-big'));

        $addedImages = [];
        for ($i = 0; $i < $imagesCount; $i++) {
            $newImage = [
                'big' => $_POST['gallery-image-big'][$i],
                'middle' => $_POST['gallery-image-middle'][$i],
                'small' => $_POST['gallery-image-small'][$i],
                'title' => $_POST['gallery-image-title'][$i],
                'alt' => !empty($_POST['gallery-image-alt'][$i]) ? $_POST['gallery-image-alt'][$i] : $_POST['gallery-image-title'][$i],
            ];
            $addedImages[] = $newImage;
        }

        $galleryCode = $this->renderPartial('gallery/gallery', [
            'addedImages' => $addedImages,
        ]);

        $response['galleryCode'] = $galleryCode;

        echo Json::encode($response);
        Yii::$app->end();
    }

    /**
     * Загрузка картинки для галлереи
     */
    public function actionUploadGalleryImage()
    {
        $response = array('files' => []);
        $uploadedFile = UploadedFile::getInstanceByName('gallery-image');

        if (!$uploadedFile->hasError) {

            $pageAlias = Yii::$app->request->getQueryParam('page-alias');
            $savePath = Yii::getAlias('@webroot/../../statics/web/minigalleries/' . $pageAlias);
            $urlPath = '/minigalleries/' . $pageAlias;
            $domain = Yii::$app->params['app']['staticsDomain'];

            if (!is_dir($savePath)) {
                mkdir($savePath, 0777, true);
            }

            $uId = uniqid();

            $newFile = [
                'name' => '',
                'size' => '',
                'url' => '',
                'bigUrl' => '',
                'middleUrl' => '',
                'smallUrl' => '',
                'thumbnailUrl' => '',
                'deleteUrl' => '',
                'deleteType' => 'DELETE',
            ];

            $newImageName = $uId . '.' . $uploadedFile->extension;
            copy($uploadedFile->tempName, $savePath .'/'. $newImageName);
            $imageInfo = getimagesize($savePath .'/'. $newImageName);
            $k = $imageInfo[0] > $imageInfo[1] ? $imageInfo[1] / $imageInfo[0] : $imageInfo[0] / $imageInfo[1];

            //Image::thumbnail($uploadedFile->tempName, 500, 500)->save($savePath .'/'. $newImageName, ['quality' => 100]);
            $newFile['bigUrl'] = $newFile['url'] = $domain . $urlPath . '/' . $newImageName;
            $newFile['name'] = pathinfo($newImageName, PATHINFO_FILENAME);
            $newFile['deleteUrl'] = Url::to(['/pages/default/delete-gallery-image/', 'url' => $urlPath . '/' . $newImageName]);

            $newImageName = $uId . '_960.jpg';
            Image::thumbnail($uploadedFile->tempName, 960, round(960 * $k))->save($savePath .'/'. $newImageName, ['quality' => 100]);
            $newFile['middleUrl'] = $domain . $urlPath . '/' . $newImageName;

            $newImageName = $uId . '_120x120.jpg';
            Image::thumbnail($uploadedFile->tempName, 120, 120)->save($savePath .'/'. $newImageName, ['quality' => 100]);
            $newFile['smallUrl'] = $newFile['thumbnailUrl'] = $domain . $urlPath . '/' . $newImageName;

            $response['files'][] = $newFile;
        }
        echo Json::encode($response);
        Yii::$app->end();
    }

    /**
     * Удаление картинки
     */
    public function actionDeleteGalleryImage()
    {
        $request = Yii::$app->request;
        $response = array('files' => []);

        if ($request->getQueryParam('url')) {
            $response['files'][] = [pathinfo($request->getQueryParam('url'), PATHINFO_FILENAME) => true];
        }

        // todo удаление файла

        echo Json::encode($response);
        Yii::$app->end();
    }
}