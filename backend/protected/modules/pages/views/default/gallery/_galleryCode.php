<?php
/**
 * @var yii\web\View $this
 * @var $addedImages array
 */
?>

<div class="sp2" style="text-align: center;">
    <div class="fotorama" data-allowfullscreen="native" data-loop="true" data-nav="thumbs" data-click="true" data-arrows="always" style="display: inline-block" data-auto="false">
        <?php foreach ($addedImages as $eachImage): ?>
            <a href="<?php echo $eachImage['middle']; ?>" data-full="<?php echo $eachImage['big']; ?>" data-caption="<?php echo $eachImage['title']; ?>" >
                <img src="<?php echo $eachImage['small']; ?>" width="120" height="120" alt="<?php echo $eachImage['alt']; ?>" title="<?php echo $eachImage['title']; ?>"/>
            </a>
        <?php endforeach; ?>
    </div>
</div>