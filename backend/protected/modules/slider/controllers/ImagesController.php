<?php
namespace backend\modules\slider\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;
use yii\web\AccessControl;
use backend\modules\admin\components\Controller;
use backend\modules\admin\actions\crud\DeleteAction;
use common\modules\slider\models\Image;
use common\extensions\fileapi\actions\UploadAction as FileAPIUploadAction;
use common\extensions\fileapi\actions\DeleteAction as FileAPIDeleteAction;

/**
 * Основной контроллер модуля Blogs
 */
class ImagesController extends Controller
{
	/**
	 * @return string Класс основной модели контролера.
	 */
	public function getModelClass()
	{
		return Image::className();
	}

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'delete' => [
		    	'class' => DeleteAction::className(),
		    	'model' => $this->modelClass
		    ],
		    'upload-temp' => [
		        'class' => FileAPIUploadAction::className(),
		        'path' => $this->module->tempPath()
		    ],
		    'delete-temp' => [
		        'class' => FileAPIDeleteAction::className(),
		        'path' => $this->module->tempPath()
		    ]
		];
	}
}