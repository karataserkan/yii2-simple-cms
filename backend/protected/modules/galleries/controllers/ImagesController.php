<?php
namespace backend\modules\galleries\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;
use yii\web\AccessControl;
use yii\data\ActiveDataProvider;
use backend\modules\admin\components\Controller;
use backend\modules\admin\actions\crud\DeleteAction;
use backend\modules\admin\actions\grid\OrderingAction;
use common\modules\galleries\models\Image;
use common\extensions\fileapi\actions\UploadAction as FileAPIUploadAction;
use common\extensions\fileapi\actions\DeleteAction as FileAPIDeleteAction;

/**
 * Основной контроллер модуля Blogs
 */
class ImagesController extends Controller
{
	/**
	 * @return string Класс основной модели контролера.
	 */
	public function getModelClass()
	{
		return Image::className();
	}

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'delete' => [
		    	'class' => DeleteAction::className(),
		    	'model' => $this->modelClass
		    ],
		    'upload-temp' => [
		        'class' => FileAPIUploadAction::className(),
		        'path' => $this->module->tempPath()
		    ],
		    'delete-temp' => [
		        'class' => FileAPIDeleteAction::className(),
		        'path' => $this->module->tempPath()
		    ],
		    'ordering' => [
		    	'class' => OrderingAction::className(),
		    	'model' => $this->modelClass
		    ]
		];
	}

	public function actionIndex($id)
	{
		$dataProvider = new ActiveDataProvider([
			'query' => Image::find()->where(['gallery_id' => $id])
		]);

		return $this->render('index', [
			'dataProvider' => $dataProvider
		]);
	}

	/**
	 * Вывод последней работы.
	 */
	public function actionLast()
	{
		$model = Image::find()->last()->one();

		return $this->render('last', [
			'model' => $model
		]);
	}

	/**
	 * Вывод последней работы.
	 */
	public function actionUpdateLast()
	{
		$models = Image::find()->notlast()->all();

		if (Yii::$app->request->isPost && ($id = Yii::$app->request->post('id')) !== null) {
			if (($model = Image::findOne($id)) !== null) {
				$model->setScenario('update-last');
				if ($model->save(false)) {
					$this->redirect(['last']);
				}
			}
		}

		return $this->render('update-last', [
			'models' => $models
		]);
	}
}