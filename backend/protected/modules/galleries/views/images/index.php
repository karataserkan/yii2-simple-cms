<?php
/**
 * Страница всех постов
 * @var yii\base\View $this
 * @var backend\modules\galleries\models\Gallery $dataProvider
 * @var backend\modules\galleries\models\search\GallerySearch $searchModel
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\grid\CheckboxColumn;
use yii\grid\SerialColumn;
use backend\modules\admin\grid\OrderingColumn;

$this->title = 'Изобаржения';
$this->params['control'] = [
    'brandLabel' => Html::encode($this->title),
    'gridId' => 'images-grid'
];

echo GridView::widget([
    'id' => 'images-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'class' => OrderingColumn::className(),
            'attribute' => 'ordering'
        ],
        [
            'attribute' => 'name',
            'format' => 'html',
            'value' => function($model) {
                return Html::img($model->url, ['style' => 'width: 100px;']);
            }
        ],
        [
            'attribute' => 'gallery_id',
            'value' => function ($model) {
                return $model->gallery['title'];
            },
        ]
    ]
]);