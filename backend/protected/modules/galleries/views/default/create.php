<?php
/**
 * Создание поста
 * @var yii\base\View $this
 * @var backend\modules\users\models\User $model
 */

use yii\helpers\Html;
use yii\widgets\Menu;

$this->title = 'Новая галерея';
$this->params['control'] = [
    'brandLabel' => Html::encode($this->title)
];

echo $this->render('_form', [
	'model' => $model,
	'statusArray' => $statusArray,
    'categoryArray' => $categoryArray,
    'imageModel' => $imageModel
]);