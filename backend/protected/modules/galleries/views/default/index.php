<?php
/**
 * Страница всех постов
 * @var yii\base\View $this
 * @var backend\modules\galleries\models\Gallery $dataProvider
 * @var backend\modules\galleries\models\search\GallerySearch $searchModel
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\grid\CheckboxColumn;
use yii\grid\SerialColumn;
use backend\modules\admin\grid\OrderingColumn;

$this->title = 'Галереи';
$this->params['control'] = [
    'brandLabel' => Html::encode($this->title),
    'gridId' => 'galleries-grid'
];

echo GridView::widget([
    'id' => 'galleries-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'class' => CheckboxColumn::classname()
        ],
        [
            'class' => OrderingColumn::className(),
            'attribute' => 'ordering'
        ],
        [
            'attribute' => 'title',
            'format' => 'html',
            'value' => function ($model) {
                return Html::a($model['title'], ['view', 'id' => $model['id']]);
            },
        ],
        [
            'attribute' => 'category_id',
            'value' => function ($model) {
                return $model->category['title'];
            },
        ],
        [
            'attribute' => 'status_id',
            'value' => function ($model) {
                return $model->status;
            },
            'filter' => Html::activeDropDownList($searchModel, 'status_id', $statusArray, ['class' => 'form-control', 'prompt' => 'Статус'])
        ],
        [
            'class' => ActionColumn::className(),
            'header' => 'Управление',
            'template' => '{view} {update} {delete} {images}',
            'buttons' => [
                'images' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-picture"></span>', ['/galleries/images/index', 'id' => $model['id']], [
                        'title' => 'Изображения',
                        'data-pjax' => '0',
                    ]);
                }
            ]
        ]
    ]
]);