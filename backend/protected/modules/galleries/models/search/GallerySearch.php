<?php
namespace backend\modules\galleries\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\galleries\models\Gallery;
use common\modules\galleries\modules\categories\models\Category;

/**
 * Общая модель поиска по галереям
 */
class GallerySearch extends Model
{
	public $id;
	public $category_id;
	public $title;
	public $status_id;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['title', 'category_id'], 'string'],
			[['status_id'], 'boolean'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'title' => 'Заголовок',
			'category_id' => 'Категория',
			'status_id' => 'Статус'
		];
	}

	public function search($params)
	{
		$joinTable = Category::tableName();
		$query = Gallery::find()->with('category')->innerJoin($joinTable, Gallery::tableName() . '.category_id = ' . $joinTable . '.id')->orderBy('ordering ASC');
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$this->addCondition($query, 'id');
		$this->addCondition($query, 'title', true);
		$this->addCondition($query, 'status_id');
		$this->addWithCondition($query, 'category_id', true);
		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', Gallery::tableName() . '.' . $attribute, $value]);
		} else {
			$query->andWhere([Gallery::tableName() . '.' . $attribute => $value]);
		}
	}

	protected function addWithCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', Category::tableName() . '.title', $value]);
		} else {
			$query->andWhere([Category::tableName() . '.title' => $value]);
		}
	}
}
