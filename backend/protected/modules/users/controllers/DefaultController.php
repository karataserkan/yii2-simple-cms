<?php
namespace backend\modules\users\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\AccessControl;
use yii\web\Response;
use yii\web\HttpException;
use yii\widgets\ActiveForm;
use backend\modules\admin\components\Controller;
use backend\modules\users\models\search\UserSearch;
use backend\modules\admin\actions\crud\IndexAction;
use backend\modules\admin\actions\crud\ViewAction;
use backend\modules\admin\actions\crud\CreateAction;
use backend\modules\admin\actions\crud\UpdateAction;
use backend\modules\admin\actions\crud\DeleteAction;
use backend\modules\admin\actions\crud\BatchDeleteAction;
use common\modules\users\models\User;
use common\modules\users\models\LoginForm;

/**
 * Основной контроллер backend-модуля [[Users]]
 */
class DefaultController extends Controller
{
	/**
	 * @return string Класс основной модели контролера.
	 */
	public function getModelClass()
	{
		return User::className();
	}

	/**
	 * @return string Класс основной поисковой модели контролера.
	 */
	public function getSearchModelClass()
	{
		return UserSearch::className();
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['access']['rules'][] = [
		    'allow' => true,
		    'actions' => ['login'],
		    'roles' => ['?']
		];
		$behaviors['access']['rules'][] = [
		    'allow' => true,
		    'actions' => ['logout'],
		    'roles' => ['@']
		];
		return $behaviors;
	}

	/**
	 * @inheritdoc
	 */
	public function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			// Запрещаем редактирование супер-админа другим админам.
			if ($action->id === 'update' && Yii::$app->request->getQueryParam('id') == $this->module->superAdminId && Yii::$app->user->id != $this->module->superAdminId ) {
				throw new HttpException(403);
			}
			return true;
		}
		return false;
	}

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
		    'index' => [
		    	'class' => IndexAction::className(),
		    	'model' => $this->modelClass,
		    	'searchModel' => $this->searchModelClass,
		    	'view' => 'index',
		    	'params' => [
		    		'statusArray' => User::getStatusArray(),
		    		'roleArray' => User::getRoleArray()
		    	]
		    ],
		    'view' => [
		    	'class' => ViewAction::className(),
		    	'model' => $this->modelClass,
		    	'view' => 'view'
		    ],
		    'create' => [
		    	'class' => CreateAction::className(),
		    	'model' => $this->modelClass,
		    	'view' => 'create',
		    	'scenario' => 'admin-create',
		    	'params' => [
		    		'statusArray' => User::getStatusArray(),
		    		'roleArray' => User::getRoleArray()
		    	]
		    ],
		    'update' => [
		    	'class' => UpdateAction::className(),
		    	'model' => $this->modelClass,
		    	'view' => 'update',
		    	'scenario' => 'admin-update',
		    	'params' => [
		    		'statusArray' => User::getStatusArray(),
		    		'roleArray' => User::getRoleArray()
		    	]
		    ],
		    'delete' => [
		    	'class' => DeleteAction::className(),
		    	'model' => $this->modelClass
		    ],
		    'batch-delete' => [
		    	'class' => BatchDeleteAction::className(),
		    	'model' => $this->modelClass
		    ]
		];
	}

	/**
	 * Авторизуем пользователя
	 */
	public function actionLogin()
	{
		// В случае если пользователь не гость, то мы перенаправляем его на главную страницу.
		if (!Yii::$app->user->isGuest) {
			$this->goHome();
		}
		$this->layout = '/login';
		$model = new LoginForm(['scenario' => 'admin']);
		if ($model->load($_POST) && $model->login()) {
			// В случае успешной авторизации, перенаправляем пользователя обратно на предыдущию страницу.
			return $this->goHome();
		} elseif (Yii::$app->request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		} else {
			// Рендерим представление
			return $this->render('login', [
				'model' => $model
			]);
		}
	}

	/**
	 * Деавторизуем пользователя
	 */
	public function actionLogout()
	{
		Yii::$app->user->logout();
		return $this->redirect('login');
	}
}