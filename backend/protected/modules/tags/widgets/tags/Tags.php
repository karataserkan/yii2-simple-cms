<?php
namespace backend\modules\tags\widgets\tags;

use Yii;
use yii\helpers\Html;
use yii\web\JsExpression;
use common\modules\tags\models\Tag;
use common\extensions\select2\Select2;

/**
 * Виджет [[Tags]]
 * Выпадающий список для добавления и редактирования тэгов модели.
 * @var yii\base\Widget $this Виджет
 * 
 * Пример использования:
 * ~~~
 * echo $form->field($model, 'tagArray')->widget(Tags::className(), [
 *     'settings' => [
 *         'width' => '100%'
 *     ]
 * ]);
 * ~~~
 */
class Tags extends Select2
{
	/**
	 * @inheritdoc
	 */
	public function init()
	{
		parent::init();
		// Определяем текущий язык.
		if ($this->items === []) {
			$this->settings['tags'] = Tag::getTagsArray();
		}
		// Определяем placeholder по умолчанию.
		if (!isset($this->options['placeholder'])) {
			$this->options['placeholder'] = 'Введите тэг';
		}
		if (!isset($this->options['multiple'])) {
			$this->options['multiple'] = true;
		}
		// Определяем собственное правило создания нового тэга.
		$this->settings['createSearchChoice'] = new JsExpression('function(term, data) {
			if (jQuery(data).filter(function() {
				return this.text.localeCompare(term) === 0;
			}).length === 0) {
				return {id : "{%#=#%}" + term, text : term};
			}
		}');
	}

	/**
	 * @inheritdoc
	 */
	public function run()
	{
		$this->registerClientScript();
		if ($this->hasModel()) {
			return Html::activeHiddenInput($this->model, $this->attribute, $this->options);
		} else {
			return Html::hiddenInput($this->name, $this->value, $this->options);
		}
  	}
}